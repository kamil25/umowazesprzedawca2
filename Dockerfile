FROM tomcat:7.0-jre8-slim

RUN rm -rf /usr/local/tomcat/webapps/ROOT
COPY target/umowazesprzedawca.war /usr/local/tomcat/webapps/ROOT.war
COPY target/umowazesprzedawca /usr/local/tomcat/webapps/ROOT


