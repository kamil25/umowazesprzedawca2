package com.kmg.umowazesprzedawca.handler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.binding.message.MessageBuilder;
import org.springframework.binding.message.MessageContext;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import com.kmg.umowazesprzedawca.model.RegisterModel;
import com.kmg.umowabackend.dao.UserDAO;
import com.kmg.umowabackend.dto.Address;

import com.kmg.umowabackend.dto.User;
import com.kmg.umowabackend.mail.EmailService;

@Component
public class RegisterHandler {


 @Autowired
 private PasswordEncoder passwordEncoder;
	
 
 private EmailService emailService;
	
 @Autowired
 private UserDAO userDAO;
 
 public RegisterModel init() { 
  return new RegisterModel();
 } 
 public void addUser(RegisterModel registerModel, User user) {
	 user.setRole("USER");
	 user.setEnabled(false);
	 
	 
	 
	 registerModel.setUser(user);
 } 
 public void addBilling(RegisterModel registerModel, Address billing) {
  registerModel.setBilling(billing);
 }

 public String validateUser(User user, MessageContext error) {
  String transitionValue = "success";
   if(!user.getPassword().equals(user.getConfirmPassword())) {
    error.addMessage(new MessageBuilder().error().source(
      "confirmPassword").defaultText("Hasła nie są identyczne!").build());
    transitionValue = "failure";    
   }  
   if(userDAO.getByEmail(user.getEmail())!=null) {
    error.addMessage(new MessageBuilder().error().source(
      "email").defaultText("Email jest już w bazie!").build());
    transitionValue = "failure";
   }
  return transitionValue;
 }
 
 public String saveAll(RegisterModel registerModel) {
  String transitionValue = "success";
  User user = registerModel.getUser();
  if(user.getRole().equals("USER")) {
   
  }
   
  // encode the password
  user.setPassword(passwordEncoder.encode(user.getPassword()));
  
  // save the user
  userDAO.add(user);
  
  
  String userMail = user.getEmail();
  String userCode = user.getCode();
  
  
  // save the billing address
  Address billing = registerModel.getBilling();
  billing.setUserId(user.getId());
  billing.setBilling(true);  
  userDAO.addAddress(billing);
  
  // dodawanie danych sprzedawcy do istniejacych umow
  
  
  
  	// send confirm mail	
  EmailService.main(userMail, userCode);
  
  return transitionValue ;
 } 
}
