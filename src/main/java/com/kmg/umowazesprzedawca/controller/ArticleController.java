package com.kmg.umowazesprzedawca.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ArticleController {

    private final static Logger logger = LoggerFactory.getLogger(ArticleController.class);

    @RequestMapping(value = "/artykuly")
    public ModelAndView artykuly() {
        ModelAndView mv = new ModelAndView("page");
        mv.addObject("title","Artykuly");
        mv.addObject("userClickArticles",true);
        return mv;
    }
    @RequestMapping(value = "/sposoby-oszustow")
    public ModelAndView art1() {
        ModelAndView mv = new ModelAndView("page");
        mv.addObject("title","sposoby-oszustow");
        mv.addObject("userClickArt1",true);
        return mv;
    }
    @RequestMapping(value = "/grupy-sprzedazowe")
    public ModelAndView art2() {
        ModelAndView mv = new ModelAndView("page");
        mv.addObject("title","grupy-sprzedazowe");
        mv.addObject("userClickArt2",true);
        return mv;
    }


}
