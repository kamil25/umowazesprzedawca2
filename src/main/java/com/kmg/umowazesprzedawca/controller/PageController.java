package com.kmg.umowazesprzedawca.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import java.io.PrintWriter;
import java.io.StringWriter;


import com.kmg.umowazesprzedawca.util.FileUtil;

import com.kmg.umowabackend.dao.UmowaDAO;

import com.kmg.umowabackend.dto.Umowa;
import com.kmg.umowabackend.mail.EmailKontaktowy;

@Controller
public class PageController {
	
	private static final Logger logger = LoggerFactory.getLogger(PageController.class);
	
	
	@Autowired
	private UmowaDAO umowaDAO;
	
	private EmailKontaktowy emailKontaktowy;
	
	@RequestMapping(value = {"","/", "/home", "/index"})
	public ModelAndView index(@RequestParam(name="logout",required=false)String logout) {		
		ModelAndView mv = new ModelAndView("page");		
		mv.addObject("title","Home");
		
		logger.info("Inside PageController index method - INFO");
		logger.debug("Inside PageController index method - DEBUG");
		
		//passing the list of categories
		//mv.addObject("categories", categoryDAO.list());
		
		
		if(logout!=null) {
			mv.addObject("message", "Zostałeś wylogowany!");			
		}
		
		mv.addObject("userClickHome",true);
		return mv;				
	}
	
	@RequestMapping(value = "/about")
	public ModelAndView about() {		
		ModelAndView mv = new ModelAndView("page");		
		mv.addObject("title","About Us");
		mv.addObject("userClickAbout",true);
		return mv;				
	}	
	
	
	@RequestMapping(value = "/.well-known/pki-validation/godaddy.html")
	public ModelAndView ssl() {		
		ModelAndView mv = new ModelAndView("godaddy");		
		mv.addObject("title","ssl");
		mv.addObject("userClickSsl",true);
		return mv;				
	}
	
	@RequestMapping(value = "/contact")
	public ModelAndView contact() {		
		ModelAndView mv = new ModelAndView("page");
		mv.addObject("title","Contact Us");
		mv.addObject("userClickContact",true);
		return mv;				
	}
	
	@RequestMapping(value = "/contact/send/{imieKlienta}/{emailKlienta}/{trescWiadomosci}")
	@ResponseBody
	public ModelAndView contactSend(@PathVariable String imieKlienta, @PathVariable String emailKlienta, @PathVariable String trescWiadomosci) {		
		
		
		//String imieKlienta, String emailKlienta, String trescWiadomosci
		String imieKlienta2 = imieKlienta;
		String emailKlienta2 = emailKlienta;
		String tresc = trescWiadomosci;
		EmailKontaktowy.main(imieKlienta2, emailKlienta2, tresc);
		
		ModelAndView mv = new ModelAndView("page");		
		mv.addObject("title","Contact Us");
		mv.addObject("userClickContact",true);
		
		return mv;				
	}
	
	@RequestMapping(value = "/regulamin")
	public ModelAndView regulamin() {		
		ModelAndView mv = new ModelAndView("page");		
		mv.addObject("title","Regulamin");
		mv.addObject("userClickRegulamin",true);
		return mv;				
	}
	
	
	
	@RequestMapping(value = "/show/all/umowy")
	public ModelAndView showAllUmowy() {		
		
		ModelAndView mv = new ModelAndView("page");		
		mv.addObject("title","All Umowy");
		
		//passing the list of categories
		//mv.addObject("categories", categoryDAO.list());
		
		mv.addObject("userClickAllUmowy",true);
		return mv;	
		
		
	}
	
	
	@RequestMapping(value="/membership")
	public ModelAndView register() {
		ModelAndView mv= new ModelAndView("page");
		
		logger.info("Page Controller membership called!");
		
		return mv;
	}
	
	
	@RequestMapping(value="/login")
	public ModelAndView login(@RequestParam(name="error", required = false)	String error,
			@RequestParam(name="logout", required = false) String logout) {
		ModelAndView mv= new ModelAndView("login");
		mv.addObject("title", "Login");
		if(error!=null) {
			mv.addObject("message", "Nieprawidłowy email lub hasło!");
		}
		if(logout!=null) {
			mv.addObject("logout", "Zostałeś wylogowany!");
		}
		return mv;
	}
	
	
	
	@RequestMapping(value="/logout")
	public String logout(HttpServletRequest request, HttpServletResponse response) {
		// Invalidates HTTP Session, then unbinds any objects bound to it.
	    // Removes the authentication from securitycontext 		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	    if (auth != null){    
	        new SecurityContextLogoutHandler().logout(request, response, auth);
	    }
	    
	    
		return "redirect:/login?logout";
	}	
	
	
	@RequestMapping(value="/access-denied")
	public ModelAndView accessDenied() {
		ModelAndView mv = new ModelAndView("error");		
		mv.addObject("errorTitle", "Przykro nam");		
		mv.addObject("errorDescription", "nie masz uprawnień do oglądania tej strony");		
		mv.addObject("title", "403 Access Denied");		
		return mv;
	}	
		
	
	
}
