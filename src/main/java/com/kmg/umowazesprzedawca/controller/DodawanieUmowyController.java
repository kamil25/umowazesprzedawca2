package com.kmg.umowazesprzedawca.controller;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.kmg.umowabackend.dao.AddressDAO;

import com.kmg.umowabackend.dao.UmowaDAO;
import com.kmg.umowabackend.dao.UserDAO;
import com.kmg.umowabackend.dto.Address;
import com.kmg.umowabackend.dto.Umowa;
import com.kmg.umowabackend.dto.User;
import com.kmg.umowabackend.mail.EmailDoKupujacego;
import com.kmg.umowabackend.mail.EmailDoSprzedajacego;
import com.kmg.umowabackend.mail.EmailUmowaAccept;

@Controller
@RequestMapping("/create")
public class DodawanieUmowyController {

	private static final Logger logger = LoggerFactory.getLogger(DodawanieUmowyController.class);

	@Autowired
	private UmowaDAO umowaDAO;
	
	@Autowired
	private UserDAO userDAO;
	@Autowired
	private AddressDAO addressDAO;
	private Address address;
	
	private User user;
	
	private EmailDoKupujacego emailDoKupujacego;
	private EmailDoSprzedajacego emailDoSprzedajacego;
	
	
	

	@RequestMapping("/umowa")
	public ModelAndView manageUmowa(@RequestParam(name="success",required=false)String success) {		

		
		
		
		ModelAndView mv = new ModelAndView("page");	
		mv.addObject("title","Dodawanie umowy");		
		mv.addObject("userClickDodawanieUmowy",true);
		
		
		
		Umowa nUmowa = new Umowa();

		mv.addObject("umowa", nUmowa);

		if(success != null) {
			if(success.equals("umowa")){
				mv.addObject("message", "Umowa pomyślnie dodana!");
			}	
			
		}
			
			
		return mv;
		
	}
	
	@RequestMapping("/{id}/umowa")
	public ModelAndView manageUmowaEdit(@PathVariable int id) {		

		ModelAndView mv = new ModelAndView("page");	
		mv.addObject("title","Dodawanie umowy");		
		mv.addObject("userClickDodawanieUmowy",true);
		
		// Product nProduct = new Product();		
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		user = userDAO.getByEmail(authentication.getName());
	    int userId = user.getId();
	    int umowaId = umowaDAO.get(id).getUmowa_UserId();
		
	    if(userId == umowaId) {
		mv.addObject("umowa", umowaDAO.get(id));
		}
	    else {
	    	// ma sie wyswietlic - nie masz uprawnien
	    }
			
		return mv;
		
	}
	
	@RequestMapping(value = "/umowa", method=RequestMethod.POST)
	public String managePostUmowa(@Valid @ModelAttribute("umowa") Umowa mUmowa, 
			BindingResult results, Model model, HttpServletRequest request) {
		
		
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		
		/*if(results.hasErrors()) {
			model.addAttribute("message", "Validation fails for adding the product!");
			model.addAttribute("userClickDodawanieUmowy",true);
			return "page";
		}*/		
		
		user = userDAO.getByEmail(authentication.getName());
	    int userId = user.getId();
	    String kupujacyEmail = user.getEmail();
	    address = addressDAO.getByUserId(userId);
	    //mUmowa.setActive(true);
	    //mUmowa.setUmowa_UserId(userId);
	    //mUmowa.setKupujacyEmail(kupujacyEmail);
	    //mUmowa.setSprzedajacyEmail(kupujacyEmail);
	    
	    String userEmail = user.getEmail();
	    String userImie = user.getFirstName();
	    String userNazwisko = user.getLastName();
	    String userPesel = user.getPesel();
	    String userUlica = address.getAddressLineOne();
	    String userNr = address.getAddressLineTwo();
	    String userMiasto = address.getCity();
	    String userNrTelefonu = user.getContactNumber();
	    
	    
	    if(mUmowa.getId() == 0 ) {
	    	mUmowa.setImieKupujacego(userImie);
	    	
	    	mUmowa.setNazwiskoKupujacego(userNazwisko);
	    	mUmowa.setPeselKupujacego(userPesel);
	    	mUmowa.setUlicaKupujacego(userUlica);
	    	mUmowa.setNrKupujacego(userNr);
	    	mUmowa.setMiastoKupujacego(userMiasto);
	    	mUmowa.setNrTelefonuKupujacego(userNrTelefonu);
	    	
	    	mUmowa.setActive(true);
		    mUmowa.setUmowa_UserId(userId);
		    mUmowa.setKupujacyEmail(kupujacyEmail);
		    mUmowa.setZaakceptowana(false);
		    
	    	umowaDAO.add(mUmowa);
	    	String umowaSprzedajacyEmail = mUmowa.getSprzedajacyEmail();
	    	EmailDoSprzedajacego.main(kupujacyEmail, umowaSprzedajacyEmail);
		}
		if(mUmowa.getId()!=0 && mUmowa.isZaakceptowana()) {
			mUmowa.setImieKupujacego(userImie);
			mUmowa.setNazwiskoKupujacego(userNazwisko);
		    mUmowa.setPeselKupujacego(userPesel);
		    mUmowa.setUlicaKupujacego(userUlica);
		    mUmowa.setNrKupujacego(userNr);
		    mUmowa.setMiastoKupujacego(userMiasto);
		    mUmowa.setNrTelefonuKupujacego(userNrTelefonu);
		    mUmowa.setUmowa_UserId(userId);
		    mUmowa.setKupujacyEmail(kupujacyEmail);
		    umowaDAO.update(mUmowa);
		}
		
		
		return "redirect:/create/umowa?success=umowa";
	}
	
	@RequestMapping(value = "/umowa/{id}/activation", method=RequestMethod.GET)
	@ResponseBody
	public ModelAndView createPostUmowaActivation(@PathVariable int id) {		
		
		ModelAndView mv = new ModelAndView("page");	
		mv.addObject("title","Lista umow sprzedajacy");		
		mv.addObject("userClickListaUmowSprzedajacy",true);
		
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		user = userDAO.getByEmail(authentication.getName());
	    String userEmail = user.getEmail();
	    String umowaSprzedajacyEmail = umowaDAO.get(id).getSprzedajacyEmail();
	    String umowaKupujacyEmail = umowaDAO.get(id).getKupujacyEmail();
		
	    if(userEmail.equals(umowaSprzedajacyEmail)) {
	    	Umowa umowa = umowaDAO.get(id);
	    	String idUmowa = umowa.getCode();
			boolean isZaakceptowana = umowa.isZaakceptowana();
			if(isZaakceptowana == false) {
			umowa.setZaakceptowana(!isZaakceptowana);
			umowaDAO.update(umowa);
			EmailDoKupujacego.main(userEmail, umowaKupujacyEmail, idUmowa);
			}
			//return (isZaakceptowana)? "Umowa Dectivated Successfully!": "Product Activated Successfully";
		}
	    else {
	    	// ma sie wyswietlic - nie masz uprawnien
	    }
		
	    return mv;
		
	}
	
	@RequestMapping(value = "/umowa/{id}/activationKupujacy", method=RequestMethod.GET)
	@ResponseBody
	public ModelAndView createPostUmowaActivationKupujacy(@PathVariable int id, @Valid @ModelAttribute("umowa") Umowa mUmowa, 
			BindingResult results, Model model, HttpServletRequest request) {		
		
		ModelAndView mv = new ModelAndView("page");	
		mv.addObject("title","Lista umow");		
		mv.addObject("userClickListaUmow",true);
		
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		user = userDAO.getByEmail(authentication.getName());
	    String userEmail = user.getEmail();
	    String umowaSprzedajacyEmail = umowaDAO.get(id).getSprzedajacyEmail();
	    String umowaKupujacyEmail = umowaDAO.get(id).getKupujacyEmail();
		
	    if(userEmail.equals(umowaKupujacyEmail)) {
	    	Umowa umowa = umowaDAO.get(id);
	    	int idUmowa = umowa.getId();
			String idUmowaS = umowa.getCode();
			boolean isZaakceptowanaKup = umowa.isZaakceptowanaKupujacy();
			if(isZaakceptowanaKup == false) {
			umowa.setZaakceptowanaKupujacy(!isZaakceptowanaKup);
			umowaDAO.update(umowa);
			EmailUmowaAccept.main(userEmail, umowaSprzedajacyEmail, idUmowaS);
			}
			//return (isZaakceptowana)? "Umowa Dectivated Successfully!": "Product Activated Successfully";
		}
	    else {
	    	// ma sie wyswietlic - nie masz uprawnien
	    }
	    
	    return mv;
		
	}
	
	@RequestMapping(value = "/umowa/{id}/dodajSprzedajacego", method=RequestMethod.GET)
	@ResponseBody
	public ModelAndView addDaneSprzedajacego(@PathVariable int id) {		
		
		ModelAndView mv = new ModelAndView("page");	
		mv.addObject("title","Lista umow sprzedajacy");		
		mv.addObject("userClickListaUmowSprzedajacy",true);
		
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		user = userDAO.getByEmail(authentication.getName());
	    int userId = user.getId();
		address = addressDAO.getByUserId(userId);
		
		String userEmail = user.getEmail();
	    String userImie = user.getFirstName();
	    String userNazwisko = user.getLastName();
	    String userPesel = user.getPesel();
	    String userUlica = address.getAddressLineOne();
	    String userNr = address.getAddressLineTwo();
	    String userMiasto = address.getCity();
	    String userNrTelefonu = user.getContactNumber();

	    
	    String umowaSprzedajacyEmail = umowaDAO.get(id).getSprzedajacyEmail();
	    String umowaKupujacyEmail = umowaDAO.get(id).getKupujacyEmail();
		
	    if(userEmail.equals(umowaSprzedajacyEmail)) {
	    	Umowa umowa = umowaDAO.get(id);
	    	int idUmowa = umowa.getId();
			umowa.setImieSprzedajacego(userImie);
			umowa.setSprzedajacyEmail(userEmail);
			umowa.setNazwiskoSprzedajacego(userNazwisko);
			umowa.setPeselSprzedajacego(userPesel);
			umowa.setUlicaSprzedajacego(userUlica);
			umowa.setNrSprzedajacego(userNr);
			umowa.setMiastoSprzedajacego(userMiasto);
			umowa.setNrTelefonuSprzedajacego(userNrTelefonu);
			
			umowaDAO.update(umowa);
			//EmailDoKupujacego.main(userEmail, umowaKupujacyEmail, idUmowa);
			
			//return (isZaakceptowana)? "Umowa Dectivated Successfully!": "Product Activated Successfully";
		}
	    else {
	    	// ma sie wyswietlic - nie masz uprawnien
	    }
		
		return mv;
	}
	
	@RequestMapping(value = "/umowa/{id}/usun", method=RequestMethod.GET)
	@ResponseBody
	public ModelAndView usunUmowe(@PathVariable int id) {		
		
		ModelAndView mv = new ModelAndView("page");	
		mv.addObject("title","Lista umow");		
		mv.addObject("userClickListaUmow",true);
		
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		user = userDAO.getByEmail(authentication.getName());
	    String userEmail = user.getEmail();
	    String userImie = user.getFirstName();
	    
	    String umowaSprzedajacyEmail = umowaDAO.get(id).getSprzedajacyEmail();
	    String umowaKupujacyEmail = umowaDAO.get(id).getKupujacyEmail();
		
	    
	    	Umowa umowa = umowaDAO.get(id);
	    	int idUmowa = umowa.getId();
			
	    	if(!umowa.isZaakceptowana() && userEmail.equals(umowaKupujacyEmail)) {
	    		umowa.setActive(false);
	    		umowaDAO.update(umowa);
	    	}
			
			//EmailDoKupujacego.main(userEmail, umowaKupujacyEmail, idUmowa);
			
			//return (isZaakceptowana)? "Umowa Dectivated Successfully!": "Product Activated Successfully";
		
		return mv;
		
	}
	
	
}
