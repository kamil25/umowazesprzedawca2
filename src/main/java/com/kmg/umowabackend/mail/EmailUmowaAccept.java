package com.kmg.umowabackend.mail;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.File;
import java.util.Properties;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;


public class EmailUmowaAccept {

    private String host = "";
    private int port = 0;
    private String username = "";
    private String password = "";

    private String emailSprzedajacy = "";
    private String emailUser = "";
    private String idumowy;

    public EmailUmowaAccept(String host, int port, String username, String password, String emailUser, String emailSprzedajacy, String idumowy) {
        this.host = host;
        this.port = port;
        this.username = username;
        this.password = password;
        this.emailSprzedajacy = emailSprzedajacy;
        this.emailUser = emailUser;
        this.idumowy = idumowy;

        sendMail();
    }

    private void sendMail() {
        Properties prop = new Properties();
        prop.put("mail.smtp.auth", true);
        prop.put("mail.smtp.starttls.enable", "true");
        prop.put("mail.smtp.host", host);
        prop.put("mail.smtp.port", port);
        prop.put("mail.smtp.ssl.trust", host);
        Session session = Session.getInstance(prop, new Authenticator() {

            @Override
            protected PasswordAuthentication getPasswordAuthentication() {

                return new PasswordAuthentication(username, password);
            }
        });


        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("umowazesprzedawca@gmail.com"));



            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(emailSprzedajacy));

            message.setSubject("Twoja umowa na portalu www.UmowaZeSprzedawca.pl została zawarta");
            String msg = "Umowa nr"+" "+idumowy+" "+"pomi&#281;dzy"+" "+ emailUser+" "+"a"+" "+emailSprzedajacy+" "+"zosta&#322;a zawarta.";
            MimeBodyPart mimeBodyPart = new MimeBodyPart();
            mimeBodyPart.setContent(msg, "text/html");
            //MimeBodyPart attachmentBodyPart = new MimeBodyPart();
            //attachmentBodyPart.attachFile(new File("pom.xml"));
            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(mimeBodyPart);
            //multipart.addBodyPart(attachmentBodyPart);
            message.setContent(multipart);
            Transport.send(message);

            Message message2 = new MimeMessage(session);
            message2.setFrom(new InternetAddress("umowazesprzedawca@gmail.com"));



            message2.setRecipients(Message.RecipientType.TO, InternetAddress.parse(emailUser));

            message2.setSubject("Twoja umowa na portalu www.UmowaZeSprzedawca.pl została zawarta");
            String msg2 = "Umowa nr"+" "+idumowy+" "+"pomi&#281;dzy"+" "+ emailUser+" "+"a"+" "+emailSprzedajacy+" "+"zosta&#322;a zawarta.";
            MimeBodyPart mimeBodyPart2 = new MimeBodyPart();
            mimeBodyPart2.setContent(msg2, "text/html");
            //MimeBodyPart attachmentBodyPart = new MimeBodyPart();
            //attachmentBodyPart.attachFile(new File("pom.xml"));
            Multipart multipart2 = new MimeMultipart();
            multipart2.addBodyPart(mimeBodyPart);
            //multipart.addBodyPart(attachmentBodyPart);
            message2.setContent(multipart);
            Transport.send(message2);


        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public static void main(String email, String emailSprzedajacego, String idUmowy) {
        String emailUser = email;
        String emailSprzedajacy = emailSprzedajacego;
        String idumowy = idUmowy;


        new EmailUmowaAccept("smtp.gmail.com", 587, "umowazesprzedawca@gmail.com", "Gevel1990", emailUser, emailSprzedajacy, idumowy);

    }
}