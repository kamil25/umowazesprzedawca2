package com.kmg.umowabackend.mail;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.File;
import java.util.Properties;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
 

public class EmailDoSprzedajacego {
     
	 private String host = "";
     private int port = 0;
     private String username = "";
     private String password = "";
     
     private String emailUser = "";
     private String emailSprzedajacy = "";
     private int idumowy;
     
     public EmailDoSprzedajacego(String host, int port, String username, String password, String emailUser, String emailSprzedajacy) {
         this.host = host;
         this.port = port;
         this.username = username;
         this.password = password;
         this.emailUser = emailUser;
         this.emailSprzedajacy = emailSprzedajacy;
        
         
         sendMail();
     }
     
     private void sendMail() {
        Properties prop = new Properties();
        prop.put("mail.smtp.auth", true);
        prop.put("mail.smtp.starttls.enable", "true");
        prop.put("mail.smtp.host", host);
        prop.put("mail.smtp.port", port);
        prop.put("mail.smtp.ssl.trust", host);
        Session session = Session.getInstance(prop, new Authenticator() {
     
        @Override
        protected PasswordAuthentication getPasswordAuthentication() {
                
    	return new PasswordAuthentication(username, password);
            }
        });
         
      
        try {
                 Message message = new MimeMessage(session);
                 message.setFrom(new InternetAddress("umowazesprzedawca@gmail.com"));
                 message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(emailSprzedajacy));
                 message.setSubject("Masz nową umowę do zaakceptowania na portalu www.umowazesprzedawca.pl");
                 String msg = "U&#380;ytkownik" + " " + emailUser + " " + "doda&#322; umow&#281; na portalu www.umowazesprzedawca.pl w kt&#243;rej jeste&#347; stron&#261; sprzeda&#380;y."+
                         " "+"Je&#380;eli uzgodni&#322;e&#347; wcze&#347;niej z u&#380;ytkownikiem"+ " " + emailUser + " " +"zawarcie umowy kupna-sprzeda&#380;y poprzez portal www.umowazesprzedawca.pl,"+
                         " to zarejestruj si&#281; na portalu i doko&#324;cz zawieranie umowy. W przeciwnym razie zignoruj t&#261; wiadomo&#347;&#263;.";
                 MimeBodyPart mimeBodyPart = new MimeBodyPart();
                 mimeBodyPart.setContent(msg, "text/html");
                 //MimeBodyPart attachmentBodyPart = new MimeBodyPart();
                 //attachmentBodyPart.attachFile(new File("pom.xml"));
                 Multipart multipart = new MimeMultipart();
                 multipart.addBodyPart(mimeBodyPart);
                 //multipart.addBodyPart(attachmentBodyPart);
                 message.setContent(multipart);
                 Transport.send(message);
         } catch (Exception e) {
            e.printStackTrace();
        }
    }
     						
     public static void main(String kupujacyEmail, String umowaSprzedajacyEmail) {
    	 String emailUser = kupujacyEmail;
    	 String emailSprzedajacy = umowaSprzedajacyEmail;
    	 
    	 
    	 
        new EmailDoSprzedajacego("smtp.gmail.com", 587, "umowazesprzedawca@gmail.com", "Gevel1990", emailUser, emailSprzedajacy);
            
    }
 }