package com.kmg.umowabackend.daoimpl;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.kmg.umowabackend.dao.UmowaDAO;
import com.kmg.umowabackend.dto.Umowa;


@Repository("umowaDAO")
@Transactional
public class UmowaDAOImpl implements UmowaDAO {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public Umowa get(int umowaId) {
		try {			
			return sessionFactory
					.getCurrentSession()
						.get(Umowa.class,Integer.valueOf(umowaId));			
		}
		catch(Exception ex) {		
			ex.printStackTrace();			
		}
		return null;
	}

	@Override
	public List<Umowa> list() {
		return sessionFactory
				.getCurrentSession()
					.createQuery("FROM Umowa" , Umowa.class)
						.getResultList();
	}
	
	@Override
	public List<Umowa> listUserUmowy(int userId) {
		
		String query = "FROM Umowa WHERE userId = " + userId + " "+"AND is_active = TRUE";
		
		return sessionFactory
				.getCurrentSession()
					.createQuery(query, Umowa.class)
						.getResultList();
	}
	
	@Override
	public List<Umowa> listUserUmowySprzedajacy(String email) {
		
		String query = "FROM Umowa WHERE sprzedajacyemail = " + "'"+email+"'" + " "+ "AND is_active = TRUE";
		
		return sessionFactory
				.getCurrentSession()
					.createQuery(query, Umowa.class)
						.getResultList();
	}
	
	@Override
	public boolean add(Umowa umowa) {
		try {			
			sessionFactory
					.getCurrentSession()
						.persist(umowa);
			return true;
		}
		catch(Exception ex) {		
			ex.printStackTrace();			
		}		
		return false;
	}

	@Override
	public boolean update(Umowa umowa) {
		try {			
			sessionFactory
					.getCurrentSession()
						.update(umowa);
			return true;
		}
		catch(Exception ex) {		
			ex.printStackTrace();			
		}		
		return false;
	}

	@Override
	public boolean delete(Umowa umowa) {
		try {
			
			umowa.setActive(false);
			// call the update method
			return this.update(umowa);
		}
		catch(Exception ex) {		
			ex.printStackTrace();			
		}		
		return false;
	}

	@Override
	public List<Umowa> getUmowyByParam(String param, int count) {
		String query = "FROM Umowa WHERE active = true ORDER BY " + param + " DESC";
		
		return sessionFactory
					.getCurrentSession()
					.createQuery(query,Umowa.class)
					.setFirstResult(0)
					.setMaxResults(count)
					.getResultList();
					
	}

	@Override
	public List<Umowa> listActiveUmowy() {
		String selectActiveUmowy = "FROM Umowa WHERE active = :active";
		return sessionFactory
				.getCurrentSession()
					.createQuery(selectActiveUmowy, Umowa.class)
						.setParameter("active", true)
							.getResultList();
	}

	@Override
	public List<Umowa> listActiveUmowyByCategory(int categoryId) {
		String selectActiveUmowyByCategory = "FROM Umowa WHERE active = :active AND categoryId = :categoryId";
		return sessionFactory
				.getCurrentSession()
					.createQuery(selectActiveUmowyByCategory, Umowa.class)
						.setParameter("active", true)
						.setParameter("categoryId",categoryId)
							.getResultList();
	}

	@Override
	public List<Umowa> getLatestActiveUmowy(int count) {
		return sessionFactory
				.getCurrentSession()
					.createQuery("FROM Umowa WHERE active = :active ORDER BY id", Umowa.class)
						.setParameter("active", true)
							.setFirstResult(0)
							.setMaxResults(count)
								.getResultList();	
	}

	

}
