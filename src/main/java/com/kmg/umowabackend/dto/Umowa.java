package com.kmg.umowabackend.dto;


import java.io.Serializable;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.stereotype.Component;

@Component
@Entity
public class Umowa implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String code;
	@NotBlank(message = "Pole musi byc wypełnione")
	private String miejscowosc;
	
	@NotBlank(message = "Pole musi byc wypełnione")
	private String przedmiotTransakcji;
	private String link;
	private double cena;
	private String formaPlatnosci;
	@Column(name = "is_active")	
	private boolean active;
	private int userId;
	private String sprzedajacyEmail;
	private String kupujacyEmail;
	private boolean zaakceptowana;
	private boolean zaakceptowanaKupujacy = false;
	private String data;
	
	private String imieSprzedajacego="";
	private String nazwiskoSprzedajacego="";
	private String peselSprzedajacego="";
	private String ulicaSprzedajacego="";
	private String nrSprzedajacego="";
	private String miastoSprzedajacego="";
	private String nrTelefonuSprzedajacego="";
	private String nrKonta="";
	
	private String imieKupujacego;
	private String nazwiskoKupujacego;
	private String peselKupujacego;
	private String ulicaKupujacego;
	private String nrKupujacego;
	private String miastoKupujacego;
	private String nrTelefonuKupujacego;
	
	
	
	
	
	
	public Umowa() {
		
		this.code = "UMO" + UUID.randomUUID().toString().substring(26).toUpperCase();
		
	}
	
	public boolean isZaakceptowanaKupujacy() {
		return zaakceptowanaKupujacy;
		
	}

	public void setZaakceptowanaKupujacy(boolean zaakceptowanaKupujacy) {
		this.zaakceptowanaKupujacy = zaakceptowanaKupujacy;
	}



	public String getImieSprzedajacego() {
		return imieSprzedajacego;
	}



	public void setImieSprzedajacego(String imieSprzedajacego) {
		this.imieSprzedajacego = imieSprzedajacego;
	}



	public boolean isZaakceptowana() {
		return zaakceptowana;
	}

	public void setZaakceptowana(boolean zaakceptowana) {
		this.zaakceptowana = zaakceptowana;
	}

	public void ustawAkceptacje() {
		zaakceptowana = true;
	}

	public String getSprzedajacyEmail() {
		return sprzedajacyEmail;
	}



	public void setSprzedajacyEmail(String sprzedajacyEmail) {
		this.sprzedajacyEmail = sprzedajacyEmail.toLowerCase().replaceAll("\\s","");
	}



	public String getKupujacyEmail() {
		return kupujacyEmail;
	}



	public void setKupujacyEmail(String kupujacyEmail) {
		this.kupujacyEmail = kupujacyEmail;
	}



	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMiejscowosc() {
		return miejscowosc;
	}

	public void setMiejscowosc(String miejscowosc) {
		this.miejscowosc = miejscowosc;
	}

	public String getPrzedmiotTransakcji() {
		return przedmiotTransakcji;
	}

	public void setPrzedmiotTransakcji(String przedmiotTransakcji) {
		this.przedmiotTransakcji = przedmiotTransakcji;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	



	public String getLink() {
		return link;
	}



	public void setLink(String link) {
		this.link = link;
	}



	public String getData() {
		return data;
	}



	public void setData(String data) {
		this.data = data;
	}



	public String getNazwiskoSprzedajacego() {
		return nazwiskoSprzedajacego;
	}



	public void setNazwiskoSprzedajacego(String nazwiskoSprzedajacego) {
		this.nazwiskoSprzedajacego = nazwiskoSprzedajacego;
	}



	public String getPeselSprzedajacego() {
		return peselSprzedajacego;
	}



	public void setPeselSprzedajacego(String peselSprzedajacego) {
		this.peselSprzedajacego = peselSprzedajacego;
	}



	public String getUlicaSprzedajacego() {
		return ulicaSprzedajacego;
	}



	public void setUlicaSprzedajacego(String ulicaSprzedajacego) {
		this.ulicaSprzedajacego = ulicaSprzedajacego;
	}



	public String getNrSprzedajacego() {
		return nrSprzedajacego;
	}



	public void setNrSprzedajacego(String nrSprzedajacego) {
		this.nrSprzedajacego = nrSprzedajacego;
	}



	public String getMiastoSprzedajacego() {
		return miastoSprzedajacego;
	}



	public void setMiastoSprzedajacego(String miastoSprzedajacego) {
		this.miastoSprzedajacego = miastoSprzedajacego;
	}



	public String getNrTelefonuSprzedajacego() {
		return nrTelefonuSprzedajacego;
	}



	public void setNrTelefonuSprzedajacego(String nrTelefonuSprzedajacego) {
		this.nrTelefonuSprzedajacego = nrTelefonuSprzedajacego;
	}



	public String getImieKupujacego() {
		return imieKupujacego;
	}



	public void setImieKupujacego(String imieKupujacego) {
		this.imieKupujacego = imieKupujacego;
	}



	public String getNazwiskoKupujacego() {
		return nazwiskoKupujacego;
	}



	public void setNazwiskoKupujacego(String nazwiskoKupujacego) {
		this.nazwiskoKupujacego = nazwiskoKupujacego;
	}



	public String getPeselKupujacego() {
		return peselKupujacego;
	}



	public void setPeselKupujacego(String peselKupujacego) {
		this.peselKupujacego = peselKupujacego;
	}



	public String getUlicaKupujacego() {
		return ulicaKupujacego;
	}



	public void setUlicaKupujacego(String ulicaKupujacego) {
		this.ulicaKupujacego = ulicaKupujacego;
	}



	public double getCena() {
		return cena;
	}



	public void setCena(double cena) {
		this.cena = cena;
	}



	public String getFormaPlatnosci() {
		return formaPlatnosci;
	}



	public void setFormaPlatnosci(String formaPlatnosci) {
		this.formaPlatnosci = formaPlatnosci;
	}


	public String getNrKonta() {
		return nrKonta;
	}



	public void setNrKonta(String nrKonta) {
		this.nrKonta = nrKonta;
	}



	public String getNrKupujacego() {
		return nrKupujacego;
	}



	public void setNrKupujacego(String nrKupujacego) {
		this.nrKupujacego = nrKupujacego;
	}



	public String getMiastoKupujacego() {
		return miastoKupujacego;
	}



	public void setMiastoKupujacego(String miastoKupujacego) {
		this.miastoKupujacego = miastoKupujacego;
	}



	public String getNrTelefonuKupujacego() {
		return nrTelefonuKupujacego;
	}



	public void setNrTelefonuKupujacego(String nrTelefonuKupujacego) {
		this.nrTelefonuKupujacego = nrTelefonuKupujacego;
	}



	public int getUmowa_UserId() {
		return userId;
	}



	public void setUmowa_UserId(int umowa_userId) {
		this.userId = umowa_userId;
	}



	@Override
	public String toString() {
		return "Umowa [id=" + id + ", code=" + code + ", miejscowosc=" + miejscowosc + ", przedmiotTransakcji="
				+ przedmiotTransakcji + ", active=" + active +  ", userId=" + userId + "]";
	}



	
	
	
	
	
	
	
	
	

}
