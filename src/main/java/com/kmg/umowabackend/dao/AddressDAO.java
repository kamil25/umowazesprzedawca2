package com.kmg.umowabackend.dao;

import java.util.List;

import com.kmg.umowabackend.dto.Address;

public interface AddressDAO {

	
	Address get(int addressId);
	Address getByUserId(int userId);
	List<Address> list();	
	boolean add(Address address);
	boolean update(Address address);
	boolean delete(Address address);
	
	

	List<Address> getAdresByParam(String param, int count);	
	
	
	// business methods
	List<Address> listActiveAdresy();
	List<Address> listUserAdresy(int userId);
	List<Address> listUserAdresySprzedajacy(String email);
	List<Address> listActiveAdresyByCategory(int categoryId);
	List<Address> getLatestActiveAdresy(int count);
	List<Address> getAddressUserId(int userId);
}
