package com.kmg.umowabackend.dao;

import java.util.List;


import com.kmg.umowabackend.dto.Umowa;

public interface UmowaDAO {

	Umowa get(int umowaId);
	List<Umowa> list();	
	boolean add(Umowa umowa);
	boolean update(Umowa umowa);
	boolean delete(Umowa umowa);

	List<Umowa> getUmowyByParam(String param, int count);	
	
	
	// business methods
	List<Umowa> listActiveUmowy();
	List<Umowa> listUserUmowy(int userId);
	List<Umowa> listUserUmowySprzedajacy(String email);
	List<Umowa> listActiveUmowyByCategory(int categoryId);
	List<Umowa> getLatestActiveUmowy(int count);
	
}
