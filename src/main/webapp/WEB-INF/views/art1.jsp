﻿<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8"%>
<link href="${css}/blog.css" rel="stylesheet">
</head>
<div class="blog-header">
    <div class="container">
        <h1 class="blog-title">Blogi i felietony</h1>
        <p class="lead blog-description">Artykuły, które odkryją przed Tobą tajniki zakupów online</p>
    </div>
</div>

<div class="container">

    <div class="row">

        <div class="col-sm-8 blog-main">

            <div class="blog-post">
                <h2 class="blog-post-title">Sposoby oszustów na uwiarygodnienie się na OLX!</h2>
                <p class="blog-post-meta">Grudzień 23, 2018 autor: Paweł Poniatowski</p>

                <p>Coraz częściej w mediach słyszymy o ujęciu przestępców okradających Polaków na portalach takich jak OLX. Wśród nich przewijają się również członkowie grup przestępczych,
                    co najlepiej świadczy o skali tego procederu i możliwych do osiągnięcia zysków z nielegalnej działalności. </p>
                <hr>
                <p>Coraz większa waga przykładana przez kupujących do dokładnej weryfikacji sprzedających spowodowała,
                    że oszuści używają coraz bardziej
                    wyszukanych metod oszukania nawet najbardziej czujnych osób. Jak to się odbywa?</p>

                <h3>Etap pierwszy zwodzenia: rozmowa telefoniczna</h3>
                <p>Jako osoby coraz bardziej podejrzliwe, nie decydujemy się wpłacać pieniędzy z góry bez chociażby rozmowy telefonicznej.
                    Spora część najbardziej prymitywnych oszustów wystawia ogłoszenia jedynie z możliwością pisania wiadomości.
                    Brak kontaktu telefonicznego to pierwszy element, po którym zapalić powinna się czerwona lampka.
                    Ludzie czyhający na nasze pieniądze jednak coraz częściej zakupują startery zarejestrowane na przypadkowe osoby,
                    których w Internecie jest mnóstwo. Regularne zmienianie aparatów telefonicznych i częste włączanie ich tylko w
                    sporej odległości od domu znacznie utrudnia poszukiwania.
                    Na czas wystawienia aukcji lub ogłoszenia i odbierania telefonów, aktywowane są pojedyncze telefony z kartami SIM. </p>

                <h3>Etap drugi: dowód tożsamości</h3>
                <p>Coraz częściej także w celu uniknięcia kłopotliwych pytań kupujących, lub też w odpowiedzi na nie,
                przesyłane są nam dowody tożsamości. Zwykle pochodzą one z kradzieży i są dokumentami zastrzeżonymi.
                Zdarzają się również dokumenty, które są skopiowane od tak zwanych słupów, osób które nie posiadają
                żadnego majątku i nawet kiedy oszustwo wyjdzie na jaw, nikt nie będzie ich ścigał. </p>
                <blockquote>
                    Naturalnie osoba „użyczająca” tożsamości nie ma nawet pojęcia o tym, przy jakim
                        oszustwie będzie wykorzystany ich dowód osobisty.
                        <strong>Pobierają opłatę za udostępnienie dokumentu i na tym kończy się ich rola.</strong> 
                </blockquote>

                <h3>Etap trzeci: podanie numeru konta </h3>
                <p>Konto w krajowym banku, które początkowo nie budzi absolutnie żadnych negatywnych skojarzeń,
                    dla bardzo wielu osób jest także zwyczajnym zjawiskiem. Wiele grup oszukujących na OLX pochodzi
                    jednak z zagranicy i w obecnych czasach także zdobycie naszego konta bankowego nie jest absolutnie
                    żadnym problemem. Na czarnym rynku kwitnie handel gotowymi kontami wraz z kartami płatniczymi.
                    Dzięki temu osoba oszukująca ukrywa się zwykle w kraju, z którym Polska nie ma umowy dotyczącej
                    wymiany informacji. Udaje się on zwykle do bankomatu i zwyczajnie pobiera pieniądze.
                    Polskie konto bankowe nie jest zatem absolutnie żadnym argumentem za wpłacaniem z góry.
                    W sieci istnieją jednak strony internetowe, na których sprawdzimy dokładnie każdy numer konta.
                    Każde zgłoszenie użytkowników, którzy zostali oszukani, trafia od razu do publicznej wiadomości.  </p>

                <img src="${images}/phishing.jpg" alt="oszusci" class="landingImg" style="max-width: 40%; height: auto; margin-top:6px;margin-bottom: 14px;">
                <p>Jak zatem widać, nie tylko rozmowa telefoniczna, ale również i udostępnienie dokumentów należących
                    do Polaka nie są obecnie gwarancją bezpieczeństwa. Dopóki podanie dowodu osobistego nie wiąże się
                    z podpisaniem umowy i poświadczeniem, że dokument należy do oszusta, nie ma on absolutnie żadnych
                    problemów z jego udostępnianiem. <strong>Na szczęście obecnie można, wykorzystując nowe możliwości <a href="${contextRoot}/home" target="_blank">platformy
                        podpisywania umów</a>, zawrzeć ją na odległość.</strong> Odpowiedzialność w razie oszustwa będzie znacznie większa,
                    a wszelkie warunki podpisane zostają jeszcze przed dokonaniem przelewu.
                    To kolejny etap weryfikacji, na który każdy powinien się zdecydować. </p>


            </div><!-- /.blog-post -->


            <nav class="blog-pagination">
                <a class="btn btn-outline-primary" href="${contextRoot}/artykuly">Zanim zrobisz zakupy online przeczytaj inne artykuły</a>

            </nav>

        </div><!-- /.blog-main -->

        <div class="col-sm-3 offset-sm-1 blog-sidebar">
            <div class="sidebar-module sidebar-module-inset">
                <h4>Aplikacja</h4>
                <p>Pamiętaj, że dzięki umowie online zwiększasz szanse na bezpieczne zakupy w internecie</p>
                <div class="mbr-section-btn pt-3 align-left">
                    <a class="btn btn-md btn-info display-4"
                       href="${contextRoot}/membership">Rejestracja</a>
                </div>
            </div>
        </div><!-- /.blog-sidebar -->

    </div><!-- /.row -->

</div><!-- /.container -->
