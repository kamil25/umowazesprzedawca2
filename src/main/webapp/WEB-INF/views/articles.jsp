<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8"%>
<link href="${css}/blog.css" rel="stylesheet">
</head>
    <div class="blog-header">
        <div class="container">
            <h1 class="blog-title">Blogi i felietony</h1>
            <p class="lead blog-description">Artykuły, które odkryją przed Tobą tajniki zakupów online</p>
        </div>
    </div>

<section class="features16 cid-qIp0qVn2Fg" id="features16-5"
         style="padding-top: 60px; padding-bottom: 45px; background-color: #ffffff;">
    <div class="container">
        <div class="row main align-items-center" >
            <div class="col-md-6 image-element " style="position:center">
                <div class="img-wrap">
                    <img src="${images}/mobile-phone-1917737_1920.jpg" class="img-rounded" alt="grupy-sprzedazowe"
                         title=""
                         style="width: 85%; height: 100%; object-fit: cover; object-position: center center;
							 box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">
                </div>
            </div>
            <div class="col-md-6 text-element" style="padding-top:20px;">
                <div class="text-content" >

                    <h2 class="mbr-title pt-2 mbr-fonts-style align-left display-2">Grupy sprzedażowe z Facebooka i groźni oszuści</h2>
                    <span style="color: #897c70">28-12-2018 Autor: Paweł Poniatowski</span>
                    <div class="mbr-section-text">
                        <p
                                class="mbr-text pt-3 mbr-light mbr-fonts-style align-left display-7">
                            Facebook dzięki uruchomieniu usługi Marketplace i działalności grup sprzedażowych zyskał wielu użytkowników 
                            i ułatwił sprzedaż niepotrzebnych przedmiotów obecnym użytkownikom. Na samym początku funkcjonowania tychże 
                            grup faktycznie dochodziło jedynie do sytuacji, kiedy to zwykli użytkownicy sprzedawali przedmioty ze swojego domu...</p>
                    </div>
                    <div class="mbr-section-btn pt-3 align-left">
                        <a class="btn btn-md btn-info display-4"
                           href="${contextRoot}/grupy-sprzedazowe">Czytaj dalej</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="features16 cid-qIp0qVn2Fg" id="features16-5"
         style="padding-top: 60px; padding-bottom: 45px; background-color: #ffffff;">

    <div class="container">
        <div class="row main align-items-center" >
            <div class="col-md-6 image-element " style="position:center">
                <div class="img-wrap">
                    <img src="${images}/text1Small.jpg" class="img-rounded" alt="rejestracja"
                         title=""
                         style="width: 85%; height: 100%; object-fit: cover; object-position: center center;
							 box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">
                </div>
            </div>
            <div class="col-md-6 text-element" style="padding-top:20px;">
                <div class="text-content" >

                    <h2 class="mbr-title pt-2 mbr-fonts-style align-left display-2">Sposoby oszustów na uwiarygodnienie się na OLX</h2>
                    <span style="color: #897c70">23-12-2018 Autor: Paweł Poniatowski</span>
                    <div class="mbr-section-text">
                        <p
                                class="mbr-text pt-3 mbr-light mbr-fonts-style align-left display-7">
                            Coraz częściej w mediach słyszymy o ujęciu przestępców okradających Polaków na portalach takich jak OLX.
                            Wśród nich przewijają się również członkowie grup przestępczych, co najlepiej świadczy o skali tego
                            procederu i możliwych do osiągnięcia zysków z nielegalnej działalności... </p>
                    </div>
                    <div class="mbr-section-btn pt-3 align-left">
                        <a class="btn btn-md btn-info display-4"
                           href="${contextRoot}/sposoby-oszustow">Czytaj dalej</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </section>
    

