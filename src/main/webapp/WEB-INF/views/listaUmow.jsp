<script src="${js}/angular.js"></script>
<script src="${js}/jspdf.min.js"></script>
<script src="${js}/jspdf.debug.js"></script>
<script src="${js}/jspdf.customfonts.min.js"></script>
<script src="${js}/default_vfs.js"></script>
<script src="${js}/umowaController.js"></script>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>	
<c:set var="contextRoot" value="${pageContext.request.contextPath}" />
<script>


</script>

</head>
<body>


	<div class="container" data-ng-app="ShoppingApp2"
		data-ng-controller="UmowaController as uCtrl">


		<div class="row" data-ng-init="uCtrl.fetchUmowy(umowa.id)">

			<div class="col-md-12">

				<div class="row" style="margin-bottom:15px;margin-top:15px;">
					<div class="col-xs-12">
						<div class="mbr-section-text">
							<b 
							id="uwaga2" style="color:red;margin-bottom:15px;" class="mbr-text pt-3 mbr-light mbr-fonts-style align-left mbr-white display-5">
								Ważne: przed zaakceptowaniem umowy, upewnij si&#281;, czy sprzedaj&#261;cy doda&#322; swoje dane 
								do umowy(w pliku PDF pojawi&#261; si&#281; jego dane) i j&#261; zaakceptowa&#322;. Ty podejmujesz ostateczn&#261; decyzj&#281; czy 
								umowa zostanie zawarta.</b>
						</div>

					</div>
				</div>

				<div class="row is-table-row">

					<div class="col-sm-6" data-ng-repeat="umowa in uCtrl.mojeUmowy" >


						<div class="thumbnail" style="border: 1px solid #8cb2bc;
    					border-radius: 5px;
    					background-color: #f4f9fa;
    					box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">
						
						
							<div class="panel-heading" style="background-color:#4582ec;">

							<h4 id="przedmiot" style="text-align: center;color:white;">
									{{umowa.przedmiotTransakcji}}</h4>

							</div>
							<div style="padding: 9px;">
							<h5 id="id" style="border:1px solid #8cb2bc;border-radius: 5px; padding:6px;background-color: white;"
							>numer umowy: <span style="color:#14467a">{{umowa.code}}</span></h5>
							<h5 id="link" style="border:1px solid #8cb2bc;border-radius: 5px; padding:6px;background-color: white;"
							>link do przedmiotu lub opis: <a href="{{umowa.link}}" target="_blank">{{umowa.link}}</a></h5>
							<h5 id="cena" style="border:1px solid #8cb2bc;border-radius: 5px; padding:6px;background-color: white;"
							>Cena: <span style="color:#14467a">{{umowa.cena}} zł</span></h5>
							<h5 id="formaPlatnosci" style="border:1px solid #8cb2bc;border-radius: 5px; padding:6px;background-color: white;"
							>Forma p&#322;atno&#347;ci: <span style="color:#14467a">{{umowa.formaPlatnosci}}</span></h5>
							<h5 id="sprzedajacyEmail" style="border:1px solid #8cb2bc;border-radius: 5px; padding:6px;background-color: white;"
							>Email sprzedaj&#261;cego: <span style="color:#14467a">{{umowa.sprzedajacyEmail}}</span></h5>
							<h5 id="zaakceptowana{{umowa.id}}" style="border:1px solid #8cb2bc;border-radius: 5px; padding:6px;background-color: white;"
							>Zaakceptowana przez sprzedaj&#261;cego: <button type="button" class="btn btn-primary btn-sm"
								data-ng-click="uCtrl.accept(umowa.id)" id="button">Sprawdź</button></h5>
							<h5 id="zaakceptowanaKupujacy{{umowa.id}}" style="border:1px solid #8cb2bc;border-radius: 5px; padding:6px;background-color: white;"
							>Zaakceptowana przez kupuj&#261;cego: <button type="button" class="btn btn-primary btn-sm"
								data-ng-click="uCtrl.accept(umowa.id)" id="button">Sprawdź</button></h5>
							
							</div>
							
							<div style="padding: 9px;">

							<button type="button" class="btn btn-primary"
								data-ng-click="uCtrl.pdf2(umowa.id)" id="button">Pobierz
								PDF</button>

						<!-- 	<a id="edytuj{{umowa.id}}" class="btn btn-info"
								href="${contextRoot}/create/{{umowa.id}}/umowa">Edytuj umow&#281;</a>
							 -->
							<a id="edytuj{{umowa.id}}" class="btn btn-info"
								data-ng-click="uCtrl.edit(umowa.id)">Edytuj umow&#281;</a>
							 
							<a id="zaakceptuj{{umowa.id}}"class="btn btn-success"
								data-ng-click="uCtrl.zaakceptuj(umowa.id)">Zaakceptuj
								umow&#281;</a> 
							<a id="usun{{umowa.id}}" class="btn btn-danger"
								data-ng-click="uCtrl.usun(umowa.id)">Usu&#324;
								umow&#281;</a>
							</div>

						</div>


					</div>



				</div>



			</div>

		</div>

	</div>


</body>
<!-- /.container -->