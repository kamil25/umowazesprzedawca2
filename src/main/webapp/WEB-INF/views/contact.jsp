<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<script src='https://www.google.com/recaptcha/api.js'></script>
<script src="${js}/contact.js"></script>
<style>
.red {
	color: red;
}

.form-area {
	background-color: #FAFAFA;
	padding: 10px 40px 60px;
	margin: 10px 0px 60px;
	border: 1px solid #4582ec;
}
</style>
<script>
	$(document).ready(function() {
		$('#characterLeft').text('140 characters left');
		$('#message').keydown(function() {
			var max = 140;
			var len = $(this).val().length;
			if (len >= max) {
				$('#characterLeft').text('You have reached the limit');
				$('#characterLeft').addClass('red');
				$('#btnSubmit').addClass('disabled');
			} else {
				var ch = max - len;
				$('#characterLeft').text(ch + ' characters left');
				$('#btnSubmit').removeClass('disabled');
				$('#characterLeft').removeClass('red');
			}
		});
	});
</script>
</head>
<body>
	<div class="container">

		<div class="col-md-5">
			<div class="form-area">
				<form role="form">
					<br style="clear: both">
					<h3 id="test" style="margin-bottom: 25px; text-align: center;">Formularz
						kontaktowy</h3>
					<div class="form-group">
						<input type="text" class="form-control" id="name" name="name"
							placeholder="Imię i nazwisko" required>
					</div>
					<div class="form-group">
						<input type="text" class="form-control" id="email" name="email"
							placeholder="Email" required>
					</div>
					<div class="form-group">
						<textarea class="form-control" type="textarea" id="message"
							placeholder="Treść wiadomości" maxlength="140" rows="7"></textarea>
						<span class="help-block"></span>
					</div>
					<div class="g-recaptcha" data-sitekey="6LcqDn8UAAAAAHetQ4xWdfzM5XPnu8jSbh0WYVHH"></div>
					<br>
					<button type="button" id="submit" onclick="contact()" name="submit"
						class="btn btn-primary pull-right" >Wyślij</button>
				</form>
			</div>
		</div>
		<div style="margin-left: auto; margin-right: auto; text-align: center; margin-top: 35px;">
			<br>
			<h3
				class="mbr-section-title align-center mbr-fonts-style mbr-bold display-2">
				<span style="font-weight: normal; margin-top: 14px;">Prosimy
					o kontakt za pośrednictwem formularza</span>
			</h3>
			<h4
				class="mbr-section-title align-center mbr-fonts-style mbr-bold display-2">
				<span style="font-weight: normal; margin-top: 14px; line-height: 1.6;">dołożymy
					wszelkich starań, aby żadno pytanie jak i żadna sugestia co do funkcjonalności aplikacji czy zgłoszony błąd w działaniu aplikacji nie pozostał bez odpowiedzi.</span>
			</h4>
			<h4
				class="mbr-section-title align-center mbr-fonts-style mbr-bold display-2">
				<span style="font-weight: normal; margin-top: 14px;">Jesteśmy również
					otwarci na propozycje współpracy.</span>
			</h4>
		</div>

	</div>
	
</body>