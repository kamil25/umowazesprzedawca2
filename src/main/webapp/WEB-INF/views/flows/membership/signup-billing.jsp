<%@include file="../flows-shared/header.jsp" %>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<link rel="shortcut icon" type="image/x-icon" href="${images}/favicon.ico" />
<title>umowa kupna-sprzedaży online</title>
<meta property="og:title" content="umowa kupna-sprzedaży online"/>
<meta property="og:image" content="https://www.umowazesprzedawca.pl/resources/images/fbLogo.png"/>
<meta property="og:site_name" content="Umowa Ze Sprzedawca"/>
<meta property="og:description" content="Umowa ze sprzedawcą - umowa kupna-sprzedaży online - bezpieczne zakupy w internecie"/>

	<div class="container">
		
		
		<div class="row">
			
			<div class="col-md-6 col-md-offset-3">
				
				<div class="panel panel-primary">
				
					<div class="panel-heading">
						<h4>Adres</h4>
					</div>
					
					<div class="panel-body">
										
						<sf:form
							method="POST"
							modelAttribute="billing"
							class="form-horizontal"
							id="billingForm"
						>
						
							
							<div class="form-group">
								<label class="control-label col-md-4" for="addressLineOne">Ulica i nr</label>
								<div class="col-md-8">
									<sf:input type="text" path="addressLineOne" class="form-control"
										/>
									<sf:errors path="addressLineOne" cssClass="help-block" element="em"/> 
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-4" for="addressLineTwo">nr mieszkania</label>
								<div class="col-md-8">
									<sf:input type="text" path="addressLineTwo" class="form-control"
										/>
									<sf:errors path="addressLineTwo" cssClass="help-block" element="em"/> 
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-4" for="city">Miasto</label>
								<div class="col-md-8">
									<sf:input type="text" path="city" class="form-control"
										/>
									<sf:errors path="city" cssClass="help-block" element="em"/> 
								</div>
							</div>
							
							<div class="form-group">
								<label class="control-label col-md-4" for="postalCode">Kod pocztowy</label>
								<div class="col-md-8">
									<sf:input type="text" path="postalCode" class="form-control"
										/>
									<sf:errors path="postalCode" cssClass="help-block" element="em"/> 
								</div>
							</div>							
						
							<div class="form-group">
								<label class="control-label col-md-4" for="state">Wojew&#243;dztwo</label>
								<div class="col-md-8">
									<sf:input type="text" path="state" class="form-control"
										/>
									<sf:errors path="state" cssClass="help-block" element="em"/> 
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-4" for="country">Kraj</label>
								<div class="col-md-8">
									<sf:input type="text" path="country" class="form-control"
										/>
									<sf:errors path="country" cssClass="help-block" element="em"/> 
								</div>
							</div>
							
							
							<div class="form-group">
								<div class="col-md-offset-4 col-md-8">
									<button type="submit" name="_eventId_personal" class="btn btn-primary">
										<span class="glyphicon glyphicon-chevron-left"></span> Wr&#243;&#263;
									</button>								
									<button type="submit" name="_eventId_confirm" class="btn btn-primary">
										Dalej<span class="glyphicon glyphicon-chevron-right"></span>
									</button>																	 
								</div>
							</div>
						
						
						</sf:form>					
					
					
					</div>
				
				
				</div>
			
			
			</div>
		
		
		</div>
		
		
	</div>

<%@include file="../flows-shared/footer.jsp" %>			
