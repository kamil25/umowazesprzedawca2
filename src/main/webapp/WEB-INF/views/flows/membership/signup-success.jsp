<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@include file="../flows-shared/header.jsp" %>


<div class="container">

	<div class="row">
	
		
		<div class="col-sm-offset-4 col-sm-4">
			<div class="text-center" >		
				<h1>Witamy!</h1>
				<h3>UmowaZeSprzedawca.pl</h3>
				<h6 style="line-height: 2.1;">Aby dokończyć rejestrację przejdź na stronę www.umowazesprzedawca.pl/confirm/email i 
				wpisz kod, który został wysłany na Twojego maila</h6>
				<div>
					<a href="${contextRoot}/confirm/email" class="btn btn-lg btn-success">Potwierdz rejestracje</a>
				</div>
			</div>
		</div>
	
	
	</div>
	

</div>	
<%@include file="../flows-shared/footer.jsp" %>