<!-- DataTable Bootstrap Script -->
<script src="${js}/angular.js"></script>
<script src="${js}/jquery-1.12.3.min.js"></script>
<script src="${js}/jspdf.min.js"></script>
<script src="${js}/pdfExample.js"></script>


<script src="${js}/productsController.js"></script>
<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.css" />
<script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.js"></script>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<script>
window.addEventListener("load", function(){
	window.cookieconsent.initialise({
	  "palette": {
		"popup": {
		  "background": "#000"
		},
		"button": {
			
		  "background": "#ffffff"
		}
	  },
	  "content": {
		"message": "Wyrażam zgodę na przetwarzanie danych osobowych na zasadach określonych w polityce prywatności.\n Jeśli nie wyrażasz zgody na wykorzystywanie cookies we wskazanych w niej celach, w tym do profilowania, prosimy o wyłącznie cookies w przeglądarce lub opuszczenie serwisu.",
		"dismiss": "ZGADZAM SIĘ!",
		"link": "CZYTAJ WIĘCEJ"
	  }
	})});
</script>
</head>
<body>




	<section class="instrukcja" style="margin-left:auto; margin-right:auto;text-align:center;background:#333333 url('${images}/baner-strona3.png') no-repeat center top fixed; background-color: #333333;">
		<div class="container" style="color: white; text-shadow: 1px 1px #000000; ">
		<header>
					

					<h1
						class="mbr-section-title align-center mbr-fonts-style mbr-bold display-2" >
						<span style="font-weight: normal; margin-top:14px; text-shadow: 2px 2px 20px black, 0 0 25px black, 0 0 15px black;">Chcesz kupić przedmioty na</span>
					</h1>

                    <h2
                      class="mbr-section-title align-center mbr-fonts-style mbr-bold display-2" >
                    <span style="font-weight: normal; margin-top:14px; text-shadow: 2px 2px 20px black, 0 0 25px black, 0 0 15px black;">znanych Polskich portalach aukcyjnych czy tablicach ogłoszeń</span>
                    </h2>

					<h2
						class="mbr-section-title align-center mbr-fonts-style mbr-bold display-2" style="margin-top: 3px; 
						">
						<span style="font-weight: normal; text-shadow: 2px 2px 20px black, 0 0 25px black, 0 0 15px black;">ale boisz się przelać pieniądze nieznajomej osobie?</span>
					</h2>
					<h3
						class="mbr-section-title align-center mbr-fonts-style mbr-bold display-2" style="margin-top: 29px;">
						<span style="font-weight: normal; text-shadow: 2px 2px 20px black, 0 0 25px black, 0 0 15px black;">Zawrzyj <b>umowę kupna-sprzedaży online</b> z osobą, która wystawiła przedmiot. </span>
					</h3>

                    <h3
                        class="mbr-section-title align-center mbr-fonts-style mbr-bold display-2" style="margin-top: 29px;">
                        <span style="font-weight: normal; text-shadow: 2px 2px 20px black, 0 0 25px black, 0 0 15px black;">Opisem kupowanego przedmiotu będzie <b>link do strony</b> z danym przedmiotem.</span>
                    </h3>
					<h4
						class="headline1" style="margin-top: 29px; text-shadow: 2px 2px 20px black, 0 0 25px black, 0 0 15px black;">
						<span style="font-weight: normal;">Zaledwie kilka kroków 
						dzieli Cię od bezpiecznego zakupu:</span>
					</h4>
					<br>
				
				</header>
			</div>
	</section>

	<section class="features16 cid-qIp0qVn2Fg" id="features16-5"
		style="padding-top: 60px; padding-bottom: 45px; background-color: #ffffff;">

		<div class="container">
			<div class="row main align-items-center" >
				<div class="col-md-6 image-element " style="position:center">
					<div class="img-wrap">
						<img src="${images}/rejestracja.png" class="img-rounded" alt="rejestracja"
							title=""
							style="width: 85%; height: 100%; object-fit: cover; object-position: center center;
							 box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">
					</div>
				</div>
				<div class="col-md-6 text-element" style="padding-top:122px;">
					<div class="text-content" >

						<h2 class="mbr-title pt-2 mbr-fonts-style align-left display-2">Rejestracja</h2>
						<div class="mbr-section-text">
							<p
								class="mbr-text pt-3 mbr-light mbr-fonts-style align-left display-7">
								Podczas rejestracji podaj swoje dane osobowe niezbędne do zawarcia umowy kupna-sprzedaży online. <b>Aplikacja jest w 100% darmowa. </b>
								Za pośrednictwem aplikacji nie wykonuje się żadnych operacji bankowych.</p>
						</div>
						<div class="mbr-section-btn pt-3 align-left">
							<a class="btn btn-md btn-info display-4"
								href="${contextRoot}/membership">Rejestracja</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="features17 cid-qIp0uk8TbN" id="features17-6"
		style="padding-top: 60px; padding-bottom: 45px; background-color: #ffffff;">

		<div class="container">
			<div class="row main align-items-center">
				<div class="col-md-6 image-element">
					<div class="img-wrap">
						<img src="${images}/dodajumowe2.png" class="img-rounded" alt="dodaj-umowę"
							title=""
							style="width: 85%; height: 100%; object-fit: cover; object-position: center center;
							box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">
					</div>
				</div>
				<div class="col-md-6 text-element" style="padding-top:122px;">
					<div class="text-content">

						<h2
							class="mbr-title pt-2 mbr-fonts-style align-left mbr-white display-2">Dodawanie umowy</h2>
						<div class="mbr-section-text">
							<p
								class="mbr-text pt-3 mbr-light mbr-fonts-style align-left mbr-white display-5">
								Podaj informacje dotyczące kupowanego przedmiotu: nazwę, link do strony/aukcji lub opis produktu itd.
								Po uzupełnieniu, sprzedający zostanie poinformowany drogą mailową o oczekującej umowie kupna-sprzedaży.</p>
						</div>
						<div class="mbr-section-btn pt-3 align-left">
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<section class="features17 cid-qIp0uk8TbN" id="features17-6"
		style="padding-top: 60px; padding-bottom: 45px; background-color: #ffffff;">

		<div class="container">
			<div class="row main align-items-center">
				<div class="col-md-6 image-element">
					<div class="img-wrap">
						<img src="${images}/akceptacja.png" alt="akceptacja-umowy" class="img-rounded"
							title=""
							style="border: 1px solid #8cb2bc;width: 85%; height: 100%; object-fit: cover; object-position: center center;
							box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">
					</div>
				</div>
				<div class="col-md-6 text-element" style="padding-top:106px;">
					<div class="text-content">

						<h2
							class="mbr-title pt-2 mbr-fonts-style align-left mbr-white display-2">Sprzedający dodaje swoje dane do umowy</h2>
						<div class="mbr-section-text">
							<p
								class="mbr-text pt-3 mbr-light mbr-fonts-style align-left mbr-white display-5">
								gdy sprzedawca uzna, że informacje przez Ciebie podane są prawidłowe, jednym kliknięciem doda swoje dane do umowy.</p>
						</div>
						<div class="mbr-section-btn pt-3 align-left">
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<section class="features17 cid-qIp0uk8TbN" id="features17-6"
		style="padding-top: 60px; padding-bottom: 45px; background-color: #ffffff;">

		<div class="container">
			<div class="row main align-items-center">
				<div class="col-md-6 image-element">
					<div class="img-wrap">
						<img src="${images}/zaakceptowana.png" alt="umowa-zaakceptowana" class="img-rounded"
							title=""
							style="border: 1px solid #8cb2bc; width: 85%; height: 100%; object-fit: cover; object-position: center center;
							box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">
					</div>
				</div>
				<div class="col-md-6 text-element" style="padding-top:80px;">
					<div class="text-content">

						<h2
							class="mbr-title pt-2 mbr-fonts-style align-left mbr-white display-2">Finalna akceptacja umowy</h2>
						<div class="mbr-section-text">
							<p
								class="mbr-text pt-3 mbr-light mbr-fonts-style align-left mbr-white display-5">
								Aby umowa została zawarta obie strony muszą zaakceptować finalną wersję dostępną w formacie pliku PDF.
								Po akceptacji, zostaje wyłączona możliwość usunięcia i edycji umowy.</p>
							<p>Zobacz przykładową umowę kupna-sprzedaży online gotową do wydruku</p>
						</div>
						<div class="mbr-section-btn pt-3 align-left">
							<button type="button" class="btn btn-primary"
								onClick="pdf4()" id="button">Pobierz
								PDF</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<section class="features17 cid-qIp0uk8TbN" id="features17-6"
		style="padding-top: 60px; padding-bottom: 45px; background-color: #ffffff;">

		<div class="container">
			<div class="row main align-items-center">
				<div class="col-md-6 image-element">
					<div class="img-wrap">
						<img src="${images}/archiwum3.png" alt="archiwum-umów" class="img-rounded"
							title=""
							style="width: 85%; height: 100%; object-fit: cover; object-position: center center;
							box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">
					</div>
				</div>
				<div class="col-md-6 text-element" style="padding-top:70px;">
					<div class="text-content">

						<h2
							class="mbr-title pt-2 mbr-fonts-style align-left mbr-white display-2">Umowa kupna-sprzedaży do pobrania</h2>
						<div class="mbr-section-text">
							<p
								class="mbr-text pt-3 mbr-light mbr-fonts-style align-left mbr-white display-5">
								W każdej chwili masz dostęp do umów, w których występujesz jako kupujący jak i do tych w których jesteś sprzedającym.</p>

						</div>
						<div class="mbr-section-btn pt-3 align-left">
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>


</body>

<!-- /.container -->
