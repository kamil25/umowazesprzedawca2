<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<div class="container">

	<div class="row">	
		
		
		
		<h3>REGULAMIN KORZYSTANIA Z USŁUG APLIKACJI UMOWAZESPRZEDAWCA
przyjęty w dniu 23.07.2018 r.</h3>



		§ 1 Definicje

		Na potrzeby niniejszego Regulaminu wskazane poniżej pojęcia będą miały następujące znaczenie:
		1.	Aplikacja - Aplikacja UmowaZeSprzedawca umożliwiająca, w oparciu o zaproponowane w jej zasobach Wzór Dokumentu, składanie Kontrahentowi prawnie wiążących oświadczeń woli o treści uzupełnionej przez Użytkownika;
		2.	Użytkownik - osoba fizyczna, która ukończyła 18 lat i posiada pełną zdolność do czynności prawnych;
		3.	Kontrahent -  osoba fizyczna, która ukończyła 18 lat i posiada pełną zdolność do czynności prawnych;
		4.	Konto - zindywidualizowany zapis informatyczny utworzony przez Usługodawcę na rzecz Użytkownika w celu korzystania z usług świadczonych w Aplikacji oraz gromadzenia w tym celu niezbędnych informacji. Dostęp do Konta zapewnia login, którym jest adres e-mail podany podczas rejestracji i hasło;
		5.	Umowa - umowa łącząca Użytkownika i Usługodawcę zawierająca istotne warunki świadczenia usług przez Usługodawcę polegających na umożliwieniu przy wykorzystaniu Aplikacji składania Kontrahentowi prawnie wiążących oświadczeń woli, której postanowienia wraz z postanowieniami Regulaminu stanowią integralną całość i składają się na powstanie zobowiązania cywilnoprawnego;
		6.	Umowa UmowaZeSprzedawca - umowa zawarta przy użyciu Aplikacji, której stronami są Użytkownik i Kontrahent;
		7.	Dokumenty UmowaZeSprzedawca - umowy zawarte przy użyciu Aplikacji i oświadczenia woli złożone przy użyciu Aplikacji, o których mowa w ust. 6 niniejszego paragrafu;
		8.	Strony - należy przez to rozumieć łącznie Usługodawcę i Użytkownika, którzy zawarli ze sobą Umowę;
		9.	Strony Dokumentów UmowaZeSprzedawca - należy przez to rozumieć łącznie Użytkownika i Kontrahenta, którzy zawarli ze sobą Umowę UmowaZeSprzedawca
		10.	Usługodawca – UMOWAZESPRZEDAWCA.PL;
		11.	Wzory Dokumentów - dostępny w bazie Aplikacji szablon Dokumentu UmowaZeSprzedawca, przy pomocy którego Użytkownik formułuje treść Dokumentów UmowaZeSprzedawca;
		12.	Regulamin - niniejszy regulamin.

		§ 2 Postanowienia ogólne

		1.	Niniejszy regulamin określa zasady korzystania z Aplikacji w wersji Web przeznaczonej do używania w przeglądarkach internetowych.
		2.	Regulamin o którym mowa w ustępie powyżej jest regulaminem w rozumieniu art. 8 ustawy z dnia 18 lipca 2002 r. o świadczeniu usług drogą elektroniczną (t.j. Dz. U. z 2017 r., poz. 1219).
		3.	Właścicielem i operatorem Aplikacji, a także usługodawcą świadczonych za jej pośrednictwem usług jest UMOWAZESPRZEDAWCA.PL.
		4.	Usługi świadczone za pośrednictwem Aplikacji polegają na usprawnianiu składania oświadczeń woli w obrocie prawnym. Aplikacja umożliwia, w oparciu o zaproponowane przez Aplikację Wzory Dokumentów, składanie Kontrahentowi  prawnie wiążących oświadczeń woli o treści wprowadzonej przez Użytkownika.
		5.	Oświadczenie woli złożone w formie dokumentowej za pomocą Aplikacji jest równoważne w obrocie prawnym z oświadczeniem woli złożonym w formie pisemnej, o ile do jego złożenia ustawodawca nie przewidział wymogu zachowania innej formy oświadczenia woli pod rygorem nieważności. Wyrażając zgodę na złożenie oświadczenia woli Użytkownik potwierdza, że podał prawdziwe dane osobowe oraz adresowe. Oznacza to, że po kliknięciu przycisku “Zaakceptuj umowę” powstaje stosunek zobowiązaniowy o treści zgodnej z treścią oświadczenia woli, co może wiązać się dla Użytkownika z odpowiedzialnością majątkową wynikającą ze złożenia oświadczenia woli.
		6.	Usługodawca rekomenduje odpowiedzialne korzystanie z Aplikacji, ze świadomością możliwych konsekwencji zaciągniętych zobowiązań oraz zastrzega, że nie ma wpływu na treść oświadczeń woli składanych za pośrednictwem Aplikacji, a jedynie umożliwia przekazywanie oświadczeń woli pomiędzy Użytkownikami oraz udostępnia Wzory Dokumentów.
		7.	Użytkownicy decydując się na złożenie oświadczenia woli za pomocą Aplikacji mają obowiązek przed złożeniem oświadczenia woli uważnie przeczytać treść projektu Dokumentu UmowaZeSprzedawca wygenerowanego przez Aplikację oraz zrozumieć znaczenie jego zapisów.  Zawierając Umowę UmowaZeSprzedawca, Użytkownicy wyrażają zgodę na wszystkie zapisy Wzoru Dokumentu w związku z tym w przypadku gdy jakikolwiek zapis Wzoru Dokumentu lub Umowy UmowaZeSprzedawca jest dla użytkowników niezrozumiały należy odstąpić od jej zawierania lub składania oświadczenia woli danej treści.
		8.	Użytkownik Aplikacji zakładając Konto w Aplikacji wyraża zgodę na otrzymywanie ofert zawarcia Umowy UmowaZeSprzedawca od innych Użytkowników.
		9.	Użytkownik Aplikacji zakładając Konto w Aplikacji wyraża zgodę na otrzymywanie ofert handlowych od Usługodawcy.
		10.	Użytkownicy Aplikacji są zobowiązani do korzystania z Aplikacji w sposób zgodny z obowiązującym prawem i Regulaminem.
		11.	Użytkownicy Aplikacji są zobowiązani w szczególności do korzystania z Aplikacji w sposób niezakłócający jej funkcjonowania oraz korzystania z Aplikacji w sposób nieuciążliwy dla innych użytkowników oraz Usługodawcy, z poszanowaniem dóbr osobistych osób trzecich (w tym prawa do prywatności) i wszelkich innych przysługującym im praw, a także korzystania z wszelkich informacji i materiałów udostępnionych za pośrednictwem Aplikacji jedynie w zakresie dozwolonego użytku.
		12.	Użytkownicy Aplikacji są zobowiązani niezwłocznie powiadomić Usługodawcę o każdym przypadku naruszenia ich praw w związku z korzystaniem z Aplikacji.


		§ 3 Wymogi techniczne

		1.	Aplikacja dostępna jest dla użytkowników przeglądarek internetowych:
		a.	Chrome 54,
		b.	Chrome Mobile - wersja z 24.10.2016 r.
		c.	Firefox 45,
		d.	Internet Explorer 11.
		2.	Koszty transmisji danych wymaganych do korzystania z Aplikacji pokrywają jej Użytkownicy we własnym zakresie na podstawie umów zawartych z operatorami telekomunikacyjnymi lub innym dostawcą Internetu. Usługodawca nie ponosi odpowiedzialności za wysokość opłat naliczanych z tytułu wykorzystania transmisji danych niezbędnego do korzystania z Aplikacji. Usługodawca zaleca użytkownikom Aplikacji korzystanie z aplikacji lub funkcji systemu operacyjnego polegających na pomiarze przesyłanych danych.

		§ 4 Rejestracja Użytkownika

		1.	Warunkiem dostępu do zasobów Aplikacji jest rejestracja Użytkownika.
		2.	Formularz rejestracyjny dostępny jest w domenie www.UmowaZeSprzedawca.pl
		3.	Dokonanie rejestracji Użytkownika jest równoznaczne z utworzeniem Konta w Aplikacji i zawarciem Umowy pomiędzy Usługodawcą a Użytkownikiem na świadczenie usług przez Usługodawcę na zasadach określonych w niniejszym Regulaminie.
		4.	Warunkiem prawidłowej rejestracji jest wypełnienie obowiązkowych pól wskazanych w formularzu, zaakceptowanie Regulaminu i kliknięcie w pole “Zarejestruj”.
		5.	W procedurze rejestracji należy podać jako obowiązkowe następujące dane:
		a.    imię,
		b.    nazwisko,
		c.     e-mail,
		d.    hasło.
		e.    numer kontaktowy,
		f.    pesel,

		6.	Hasło musi zawierać co najmniej 8 znaków, jedną dużą literę i jedną cyfrę.
		7.	Po prawidłowym uzupełnieniu formularza rejestracyjnego i kliknięciu w pole “Zarejestruj”, wprowadzone dane są przesyłane automatycznie do Usługodawcy w celu utworzenia Konta.
		8.	Użytkownik otrzymuje na podany w formularzu rejestracyjnym adres e-mail kod aktywacyjny.
		9.	Wpisanie przesłanego kodu aktywacyjnego w polu “Aktywacja Konta” kończy rejestrację i oznacza utworzenie Konta Użytkownika w Aplikacji UmowaZeSprzedawca.
		10.	Dla skuteczności działania Aplikacji niezbędne jest podanie prawdziwych danych osobowych. Aplikacja automatycznie uzupełnia oświadczenia woli Użytkownika składane za pomocą Aplikacji o dane osobowe podane przy pierwszym uruchomieniu, z uwzględnieniem prawnych wymogów zastrzeżonych dla skuteczności czynności prawnej.

		§ 5 Logowanie Użytkownika

		1.	Użytkownik, który dokonał rejestracji Konta w sposób opisany w paragrafie poprzedzającym korzysta z zasobów Aplikacji po zalogowaniu się w domenie htttp://UmowaZeSprzedawca.pl/aplikacja
		2.	W celu zalogowania się do Aplikacji Użytkownik wpisuje e-mail oraz hasło podane przy rejestracji i wybiera pole “Zaloguj się”.

		§ 6 Dezaktywacja Konta

		1.	Użytkownik może w każdym czasie żądać dezaktywacji swojego Konta.
		2.	W przypadku o którym mowa w ust. 1 Użytkownik wysyła żądanie dezaktywacji Konta za pośrednictwem formularza dostępnego w Aplikacji.Formularz jest dostępny pod adresem https://www.UmowaZeSprzedawca.pl/mojeKonto/
		3.	Dezaktywacja Konta wiąże się z utratą możliwości zawierania Dokumentów UmowaZeSprzedawca za pośrednictwem Aplikacji, aż do momentu powtórnej aktywacji Konta.
		4.	Dezaktywacja Konta nie ma wpływu na obowiązywanie zawartych Umów oraz złożonych oświadczeń woli za pośrednictwem Aplikacji, tj. pozostają one obowiązujące zgodnie z ich treścią.



		§ 7 Procedura zawierania Dokumentów UmowaZeSprzedawca

		1.	Użytkownik przystępuje do przygotowania treści Dokumentu UmowaZeSprzedawca poprzez kliknięcie w pole o nazwie „Dodaj umowę”.
		2.	Użytkownika przystępując do tworzenia Dokumentu automatycznie staję się stroną kupna.
		3.	Połączenie Użytkownika z Kontrahentem i umożliwienie im zawarcia Umowy UmowaZeSprzedawca następuje poprzez podanie adresu email Kontrahenta
		5.	Gdy Użytkownik uzna, że wprowadzone przez niego dane są poprawne, przesyła Dokument UmowaZeSprzedawca do Kontrahenta poprzez kliknięcie w przycisk “Zapisz”.
		6.	Przesłanie Umowy UmowaZeSprzedawca do Kontrahenta w sposób opisany w ustępie 5 powyżej stanowi ofertę. Zaakceptowanie treści Umowy UmowaZeSprzedawca przez Kontrahenta w brzmieniu niezmienionym oznacza przyjęcie oferty przez Kontrahenta.
		9.	Każdorazowo o otrzymaniu ofert Użytkownik oraz Kontrahent powiadamiani są drogą mailową na adresy wskazane podczas rejestracji Konta.
		10.	Dla skutecznego zawarcia umowy UmowaZeSprzedawca wymaga się dokonania akceptacji jego treści zarówno przez Kontrahenta jak i użytkownika, poprzez kliknięcie w przycisk „zaakceptuj umowę”.

		§ 8 Dostęp Użytkownika do Dokumentów UmowaZeSprzedawca

		1.	W zakładce “Lista umów - kupno” oraz „Lista umów – sprzedaż” Użytkownikowi umożliwia się dostęp do pełnego wykazu zawartych przez niego za pomocą Aplikacji Umów UmowaZeSprzedawca, projektów Umów UmowaZeSprzedawca, których formularze znajdują się w trakcie uzupełniania, Umów UmowaZeSprzedawca oczekujących na akceptację przez Kontrahenta.

		§ 9 Prawa autorskie i pokrewne

		1.	Wszelkie prawa do Aplikacji oraz do udostępnianych treści w całości i we fragmentach, w szczególności elementów tekstowych, graficznych oraz elementów aplikacji programistycznych generujących i obsługujących Aplikację, są zastrzeżone dla Usługodawcy.
		2.	Usługodawca udziela Użytkownikowi licencji na korzystanie z treści na zasadach i w czasie określonym w § 14 niniejszego Regulaminu.
		3.	Licencja, o której mowa w ust. 2 i 3 nie upoważnia Użytkownika do udzielania dalszych licencji.
		4.	Przekazywanie osobom trzecim treści zawartych w Aplikacji dopuszczalne jest przy wykorzystaniu narzędzi zawartych w Aplikacji i przeznaczonych do tego celu.
		5.	Użytkownik nie ma prawa zwielokrotniać, sprzedawać lub w inny sposób wprowadzać do obrotu lub rozpowszechniać Aplikacji, w całości bądź we fragmentach, w szczególności przesyłać lub udostępniać jej w systemach i sieciach komputerowych, systemach dystrybucji aplikacji mobilnych lub jakichkolwiek innych systemach teleinformatycznych.

		§ 10 Zasady udostępniania treści

		1.	Usługodawca nie gwarantuje nieprzerwanej dostępności treści ani parametrów jakości dostępu do nich. Parametry jakości dostępu są uzależnione od przepustowości internetowej sieci pośredniczącej pomiędzy Usługodawcą a Użytkownikiem i Kontrahentem oraz innych czynników niezależnych od Usługodawcy.
		2.	Usługodawca zastrzega sobie prawo ograniczenia, zawieszania lub zaprzestania prezentacji treści w części lub w całości.
		3.	Korzystanie z Aplikacji lub treści w sposób sprzeczny z prawem, zasadami współżycia społecznego, dobrymi obyczajami lub niniejszym Regulaminem uzasadnia natychmiastowe zaprzestanie świadczenia Usługi dla danego Użytkownika.
		4.	Reklamacje z tytułu nieprawidłowego dostępu do Usług należy zgłaszać Usługodawcy w trybie określonym w § 16 niniejszego Regulaminu.

		§ 11 Reklamacje

		1.	Reklamacje dotyczące funkcjonowania Aplikacji i dostępu do jej treści należy zgłaszać drogą mailową na adres Usługodawcy umowazesprzedawca@gmail.com
		2.	Reklamacja powinna zawierać dane umożliwiające rozpatrzenie reklamacji, w szczególności imię i nazwisko zgłaszającego oraz adres e-mail podany przy rejestracji Konta, a także wskazywać okoliczności uzasadniające reklamację.
		3.	Usługodawca ustosunkuje się do treści reklamacji w ciągu 14 dni za pośrednictwem wiadomości e-mail przesłanej na adres wskazany w reklamacji.
		4.	Usługodawca nie rozpoznaje zastrzeżeń niezwiązanych z funkcjonowaniem Aplikacji, w szczególności uwag odnoszących się do niewykonania bądź nienależytego wykonania Umowy UmowaZeSprzedawca przez Kontrahenta. Reklamacje, których przedmiotem są okoliczności wskazane w zdaniu poprzednim, Usługodawca ma prawo pozostawić bez odpowiedzi.

		§ 12 Umowa

		1.	Z chwilą dokonania rejestracji w Aplikacji pomiędzy Użytkownikiem a Usługodawcą zostaje zawarta Umowa na czas nieokreślony.
		2.	Usługodawca zobowiązuje się do świadczenia usług na rzecz Użytkownika polegających na umożliwieniu mu:
		a.	tworzenia projektu Umowy UmowaZeSprzedawca na bazie Wzoru dokumentu i wprowadzonych przez Użytkownika Aplikacji UmowaZeSprzedawca indywidualnych danych;
		b.	zawarcia Umowy UmowaZeSprzedawca za pośrednictwem Aplikacji;
		3.	Użytkownik zobowiązuje się do:
		a.	podania w procesie rejestracji prawdziwych danych;
		b.	korzystania z Aplikacji w sposób niezakłócający jej funkcjonowania;
		c.	korzystania z Aplikacji w sposób nieuciążliwy dla innych użytkowników oraz Usługodawcy, z poszanowaniem dóbr osobistych osób trzecich (w tym prawa do prywatności) i wszelkich innych przysługującym im praw;
		d.	korzystania z wszelkich informacji i materiałów udostępnionych za pośrednictwem Aplikacji jedynie w zakresie dozwolonego użytku.
		4.	Użytkownik może w każdej chwili zakończyć korzystanie z usług świadczonych przez Usługodawcę poprzez Dezaktywację Konta.
		5.	Użytkownik, który w sposób wskazany w ust. 1 zawarł Umowę z Usługodawcą, może od niej odstąpić w terminie 14 dni od zawarcia Umowy, pod warunkiem, że nie rozpoczął jeszcze korzystania z usług świadczonych w Aplikacji.
		6.	Złożenie oświadczenia o odstąpieniu od Umowy dokonane z naruszeniem ust. 6 nie wywołuje skutków prawnych.



		Polityka prywatności

		I. Administrator danych osobowych
		Przekazane przez Ciebie dane osobowe są współadministrowane przez:
		UmowaZeSprzedawca.pl

		Współadministrowanie odbywa się na mocy regulacji wynikających z Art. 26 Ust. 1 RODO (Rozporządzenie Parlamentu Europejskiego i Rady (UE) 2016/679 z dnia 27 kwietnia 2016 r. w sprawie ochrony osób fizycznych w związku z przetwarzaniem danych osobowych i w sprawie swobodnego przepływu takich danych oraz uchylenia dyrektywy 95/46/WE – ogólne rozporządzenie o ochronie danych) i spowodowane jest faktem, iż podmioty te wspólnie ustalają cele i sposoby przetwarzania Twoich danych.
		II. Zasady ogólne przetwarzania danych osobowych
		Gdzie przechowujemy Twoje dane?
		Twoje dane osobowe przechowujemy na terenie Europejskiego Obszaru Gospodarczego („EOG”), lecz w myśl przepisów RODO mogą być one przesyłane także do kraju spoza tego obszaru i tam przetwarzane. Każda operacja przesyłania danych osobowych jest wykonywana zgodnie z obowiązującym prawem. Jeśli przekazujemy dane poza obszar EOG, jako współadministrator, stosujemy Standardowe klauzule umowne oraz Tarczę prywatności. Są to środki zabezpieczające w odniesieniu do krajów, w przypadku których Komisja Europejska nie stwierdziła odpowiedniego poziomu ochrony danych.
		Kto ma dostęp do Twoich danych?
		Twoje dane mogą być udostępniane jedynie w ramach podmiotów wchodzących w skład współadministrowania.
		Oznacza to, że nigdy nie przekazujemy Twoich danych, nie sprzedajemy ich, ani nie wymieniamy z innymi podmiotami w celach marketingowych.
		W niektórych przypadkach Twoje dane przekazujemy wybranym podmiotom zewnętrznym. Czynimy to jedynie w ramach realizacji usług, które zostały przez nas zdefiniowane i w zakresie, jaki obejmują podpisane przez nas umowy powierzenia przetwarzania. Szczegółowe informacje na temat kategorii podmiotów zewnętrznych znajdziesz niżej.
		Jakie są podstawy prawne przetwarzania danych?
		Przetwarzamy Twoje dane osobowe na mocy zawartej umowy, której wzorzec znajdziesz Regulaminie. Twoje dane przetwarzamy tylko w takim zakresie, jaki jest potrzebny do właściwej realizacji świadczonej dla Ciebie usługi.
		W niektórych przypadkach, na przykład lepszego dopasowania działania usług do Twoich potrzeb, możemy poprosić Cię o podanie dodatkowych danych. Wyrażenie zgody jest oczywiście dobrowolne, a wszystkie wyrażone zgody możesz odwołać.
		Uaktualnienia zasad naszej Polityki prywatności:
		W przyszłości może zajść konieczność aktualizacji Polityki prywatności. Jej najnowsza wersja będzie zawsze dostępna na naszej stronie internetowej w zakładce Polityka prywatności. Poinformujemy Cię tam o każdej zmianie Polityki prywatności. Na przykład celu, w jakim korzystamy z Twoich danych osobowych, tożsamości Inspektora Ochrony Danych Osobowych lub Twoich praw.
		III. Twoje prawa w kontekście RODO
		Jakie prawa przysługują Ci w świetle przepisów RODO?
		Prawo dostępu do danych
		W każdej chwili masz prawo otrzymać komplet informacji o tym, które z Twoich danych osobowych przechowujemy. W tym celu skontaktuj się z nami, pisząc na adres UmowaZeSprzedawca.pl@gmail.com  a my przekażemy Ci te informacje w wiadomości e-mail.
		Prawo do przenoszenia danych
		Gdy przetwarzamy Twoje dane osobowe w sposób zautomatyzowany, na podstawie Twojej zgody lub zawartej umowy, masz prawo otrzymać kopię swoich danych w ustrukturyzowanym, powszechnie używanym i możliwym do odczytania formacie. Kopia ta może zostać przesłana do Ciebie lub do innego podmiot, który odtąd będzie przetwarzał Twoje dane osobowe. Pamiętaj, że dotyczy to wyłącznie danych osobowych, które zostały nam przekazane.
		Prawo do poprawiania danych
		W ramach RODO przysługuje Ci prawo do poprawienia lub uzupełnienia Twoich danych osobowych. Jeśli posiadasz konto w sklepie www.UmowaZeSprzedawca.pl.plmożesz w każdej chwili edytować nie tylko swoje dane osobowe, ale i zgody w ramach ustawień swojego konta.
		Prawo do usunięcia danych
		W każdej chwili możesz usunąć przetwarzane przez nas Twoje dane osobowe. Dokonasz tego w dowolnym momencie, za wyjątkiem sytuacji kiedy:
		•	masz otwarte zamówienie, które nie zostało jeszcze wysłane lub zostało wysłane tylko częściowo;
		•	masz wobec nas nieuregulowaną należność, niezależnie od metody płatności;
		•	podejrzewamy lub mamy pewność, że w ciągu ostatnich czterech lat korzystałeś z naszych usług w niewłaściwy sposób;
		•	Twój wniosek kredytowy został odrzucony w ciągu ostatnich trzech miesięcy;
		•	jeśli dokonałeś jakikolwiek zakupów, wtedy zachowamy Twoje dane osobowe dotyczące transakcji w celach księgowych.


		Prawo do wniesienia sprzeciwu na przetwarzanie danych na podstawie uzasadnionego interesu
		RODO daje Ci także prawo do wniesienia sprzeciwu na przetwarzanie Twoich danych, w takim zakresie, w jakim przetwarzamy Twoje dane, opierając się o prawnie uzasadniony interes współadministratorów. Zaprzestaniemy wówczas przetwarzania Twoich danych osobowych, chyba że będziemy w stanie znaleźć zgodne z prawem uzasadnienie tego procesu, które unieważni Twój interes lub prawa; albo z powodu roszczeń prawnych. Jeśli dojdzie do takiej sytuacji poinformujemy Cię o tym fakcie.
		Prawo do niewyrażenia zgody na marketing bezpośredni
		Masz prawo nie zgodzić się na otrzymywanie materiałów marketingu bezpośredniego, w tym także na sporządzanie analizy Twojego profilu, którą przygotowujemy w celu tworzenia takich materiałów.
		Aby zrezygnować z otrzymywania materiałów marketingu bezpośredniego, wystarczy że:
		•	wykonasz instrukcję zawartą w każdej takiej wiadomości e-mail;
		•	edytujesz ustawienia swojego konta na stronie www.


		Prawo do ograniczenia przetwarzania danych osobowych
		Masz prawo zażądać, abyśmy ograniczyli przetwarzanie Twoich danych osobowych pod następującymi warunkami:
		•	jeśli nie zgodzisz się na przetwarzanie danych na podstawie uzasadnionego interesu współadministratorów. Wtedy ograniczymy jakiekolwiek przetwarzanie takich danych po zweryfikowaniu, czy istnieje taki uzasadniony interes;
		•	jeśli zgłosisz, że Twoje dane osobowe są niepoprawne. Wtedy ograniczymy jakiekolwiek przetwarzanie danych do momentu zweryfikowania ich poprawności;
		•	jeśli przetwarzanie danych jest niezgodne z prawem. Wówczas możesz nie zgodzić się na wykasowanie danych osobowych i zamiast tego zażądać ograniczenia używania Twoich danych osobowych;
		•	jeśli nie potrzebujemy już Twoich danych osobowych, ale są one wymagane do zgłoszenia lub oddalenia roszczenia.


		IV. Polityka cookies
		Cookies - oznaczają dane informatyczne, w szczególności niewielkie pliki tekstowe, zapisywane i przechowywane na urządzeniach, za pośrednictwem których korzystasz z naszych serwisów.
		Cookies Administratora - oznaczają cookies zamieszczane przez nas, związane ze świadczeniem usług drogą elektroniczną przez nas za pośrednictwem naszych serwisów.
		Cookies Zewnętrzne - oznaczają cookies zamieszczane przez naszych partnerów za pośrednictwem strony internetowej serwisu.
		Jak używamy plików cookies?
		Wykorzystujemy dwa typy plików cookies:
		•	cookies sesyjne: są przechowywane na Twoim urządzeniu i pozostają tam do momentu zakończenia sesji danej przeglądarki. Zapisane informacje są wówczas trwale usuwane z pamięci urządzenia. Mechanizm cookies sesyjnych nie pozwala na pobieranie jakichkolwiek danych osobowych ani żadnych informacji poufnych z Twojego urządzenia.
		•	cookies trwałe: są przechowywane na Twoim urządzeniu i pozostają tam do momentu ich skasowania. Zakończenie sesji danej przeglądarki lub wyłączenie urządzenia nie powoduje ich usunięcia z urządzenia użytkownika. Mechanizm cookies trwałych nie pozwala na pobieranie jakichkolwiek danych osobowych ani żadnych informacji poufnych z Twojego urządzenia. Trwałych plików cookie używamy do przechowywania informacji o Twojej preferowanej stronie startowej oraz przechowywania Twoich danych, jeśli przed zalogowaniem klikniesz opcję „Pamiętaj mnie”. Będziemy też używać plików cookies do zapisywania Twoich ulubionych produktów.


		Używamy zarówno naszych plików cookies, jak i plików cookie podmiotów zewnętrznych w celu gromadzenia statystyk oraz danych użytkowników w formie zagregowanej oraz indywidualnej. Korzystamy także z narzędzi analitycznych do optymalizacji naszej strony i przedstawiania oferty interesującej dla użytkownika.
		Używamy także zewnętrznych plików cookie, które monitorują, w jaki sposób użytkownik korzysta z innych witryn. Dzięki temu możemy udostępniać materiały marketingowe na innych stronach i w różnych kanałach komunikacji.
		Do jakich celów wykorzystujemy pliki cookies?
		Wykorzystujemy cookies w następujących celach:
		•	Konfiguracji serwisu:
		o	Dostosowania zawartości stron internetowych serwisu do preferencji użytkownika oraz optymalizacji korzystania ze stron internetowych serwisu.
		o	Rozpoznania urządzenia użytkownika serwisu oraz jego lokalizacji i odpowiedniego wyświetlenia strony internetowej, dostosowanej do jego indywidualnych potrzeb.
		o	Zapamiętania ustawień wybranych przez użytkownika i personalizacji interfejsu użytkownika, np. w zakresie wybranego języka lub regionu, z którego pochodzi użytkownik.
		o	Zapamiętania historii odwiedzonych stron w serwisie w celu rekomendacji treści.
		o	Rozmiaru czcionki, wyglądu strony internetowej itp.
		•	Uwierzytelniania użytkownika w serwisie i zapewnienia sesji użytkownika w serwisie:
		o	Utrzymania sesji użytkownika serwisu (po zalogowaniu), dzięki której użytkownik nie musi na każdej podstronie serwisu ponownie wpisywać loginu i hasła.
		o	Poprawnej konfiguracji wybranych funkcji serwisu, umożliwiając w szczególności weryfikację autentyczności sesji przeglądarki.
		o	Optymalizacji i zwiększenia wydajności usług świadczonych przez Administratora.
		•	Realizacji procesów niezbędnych dla pełnej funkcjonalności stron internetowych:
		o	Dostosowania zawartości stron internetowych serwisu do preferencji użytkownika oraz optymalizacji korzystania ze stron internetowych serwisu. W szczególności pliki te pozwalają rozpoznać podstawowe parametry urządzenia użytkownika i odpowiednio wyświetlić stronę internetową, dopasowaną do jego indywidualnych potrzeb.
		o	Poprawnej obsługi programu partnerskiego, umożliwiając w szczególności weryfikację źródeł przekierowań użytkowników na strony internetowe serwisu.
		•	Zapamiętania lokalizacji użytkownika:
		o	Poprawnej konfiguracji wybranych funkcji serwisu, umożliwiając w szczególności dostosowanie dostarczanych informacji do użytkownika z uwzględnieniem jego lokalizacji.
		•	Analiz, badań oraz audytu oglądalności oraz tworzenia anonimowych statystyk, które pomagają zrozumieć, w jaki sposób Użytkownicy serwisu korzystają ze stron internetowych serwisu, co umożliwia ulepszanie ich struktury i zawartości.
		•	Świadczenia usług reklamowych i dostosowania prezentowanych za pośrednictwem serwisu reklam produktów i usług.
		•	Zapewnienia bezpieczeństwa i niezawodności serwisu.


		Wykorzystujemy też cookies zewnętrzne w następujących celach:
		•	Zbierania ogólnych i anonimowych danych statycznych za pośrednictwem narzędzi analitycznych np. Google Analytics, Gemius Traffic.
		•	Prezentowania reklam dostosowanych do preferencji użytkownika z wykorzystaniem narzędzia internetowej reklamy.
		•	Wykorzystania funkcji interaktywnych w celu popularyzacji serwisu za pomocą serwisów społecznościowych:
		o	twitter.com
		o	Facebook.com


		Czy możesz się na to nie zgodzić?
		Oczywiście, możesz samodzielnie i w każdym momencie zmienić ustawienia dotyczące plików cookies, określając warunki ich przechowywania i uzyskiwania dostępu przez pliki cookies do Twojego urządzenia. Zmiany ustawień, o których mowa w zdaniu poprzednim, możesz dokonać za pomocą ustawień przeglądarki internetowej lub za pomocą konfiguracji usługi. Ustawienia te mogą zostać zmienione w szczególności w taki sposób, aby blokować automatyczną obsługę plików cookies w ustawieniach przeglądarki internetowej, bądź informować o ich każdorazowym zamieszczeniu na Twoim urządzeniu. Szczegółowe informacje o możliwości i sposobach obsługi plików cookies dostępne są w ustawieniach oprogramowania (przeglądarki internetowej).
		Użytkownik może w każdej chwili usunąć pliki cookies korzystając z dostępnych funkcji w przeglądarce internetowej, której używa.
		Ograniczenie stosowania plików cookies, może wpłynąć na niektóre funkcjonalności dostępne na stronie internetowej serwisu. Dane przekazywane firmom zewnętrznym są używane wyłącznie do świadczenia usług wymienionych powyżej. Używamy narzędzi analitycznych do gromadzenia danych statystycznych w celu optymalizowania naszej witryny i przedstawiania informacji, które mogą Cię zainteresować.





		</p>
	
	</div>


</div>