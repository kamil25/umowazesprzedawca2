<script src="${js}/angular.js"></script>
<script src="${js}/usersController.js"></script>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<script>
function skasuj() {

		window.open("${contextRoot}/mojeKonto/skasujKonto/");
	}
</script>
</head>
<body>

<section class="mojeKonto">
	<div class="container" data-ng-app="ShoppingApp4"
		data-ng-controller="UsersController as uCtrl">


		<div class="row" data-ng-init="uCtrl.fetchMojeKonto()">

			<div class="col-md-12">

				<div class="row" style="margin-bottom:15px;margin-top:15px;">
					<div class="col-xs-12">
						

					</div>
				</div>

				<div class="row is-table-row">

					<div class="col-sm-6" data-ng-repeat="user in uCtrl.mojeKonto" >


						<div class="thumbnail" style="border: 1px solid #8cb2bc;
    					border-radius: 5px;
    					background-color: #f4f9fa;
    					box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">
						
						
							<div class="panel-heading" style="background-color:#4582ec;">

							<h4 id="przedmiot" style="text-align: center;color:white;">
									Dane kontaktowe</h4>

							</div>
							<!--  <div class="caption" style="height: 71px;">
								<h3 id="przedmiot" style="text-align: center;">
									{{umowa.przedmiotTransakcji}}</h3>
							</div>-->
							<div style="padding: 9px;">
							<h5 id="id" style="border:1px solid #8cb2bc;border-radius: 5px; padding:6px;background-color: white;"
							>Imię: <span style="color:#14467a">{{user.firstName}}</span></h5>
							<h5 id="link" style="border:1px solid #8cb2bc;border-radius: 5px; padding:6px;background-color: white;"
							>Nazwisko: <span style="color:#14467a">{{user.lastName}}</span></h5>
							<h5 id="link" style="border:1px solid #8cb2bc;border-radius: 5px; padding:6px;background-color: white;"
							>Email: <span style="color:#14467a">{{user.email}}</span></h5>
							<h5 id="link" style="border:1px solid #8cb2bc;border-radius: 5px; padding:6px;background-color: white;"
							>Telefon kontaktowy: <span style="color:#14467a">{{user.contactNumber}}</span></h5>
							<h5 id="link" style="border:1px solid #8cb2bc;border-radius: 5px; padding:6px;background-color: white;"
							>Pesel: <span style="color:#14467a">{{user.pesel}}</span></h5>
							
							
							

						</div>


					</div>



				</div>
				</div>


			</div>

		</div>
		
		<div class="row" data-ng-init="uCtrl.fetchMojeKontoAdres()">

			<div class="col-md-12">

				<div class="row" style="margin-bottom:15px;margin-top:15px;">
					<div class="col-xs-12">
						

					</div>
				</div>

				<div class="row is-table-row">

					<div class="col-sm-6" data-ng-repeat="address in uCtrl.mojeKontoAdres" >


						<div class="thumbnail" style="border: 1px solid #8cb2bc;
    					border-radius: 5px;
    					background-color: #f4f9fa;
    					box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">
						
						
							<div class="panel-heading" style="background-color:#4582ec;">

							<h4 id="przedmiot" style="text-align: center;color:white;">
									Adres </h4>

							</div>
							<!--  <div class="caption" style="height: 71px;">
								<h3 id="przedmiot" style="text-align: center;">
									{{umowa.przedmiotTransakcji}}</h3>
							</div>-->
							<div style="padding: 9px;">
							<h5 id="id" style="border:1px solid #8cb2bc;border-radius: 5px; padding:6px;background-color: white;"
							>Ulica i numer: <span style="color:#14467a">{{address.addressLineOne}}</span></h5>
							<h5 id="id" style="border:1px solid #8cb2bc;border-radius: 5px; padding:6px;background-color: white;"
							>Numer mieszkania: <span style="color:#14467a">{{address.addressLineTwo}}</span></h5>
							<h5 id="id" style="border:1px solid #8cb2bc;border-radius: 5px; padding:6px;background-color: white;"
							>Miasto: <span style="color:#14467a">{{address.city}}</span></h5>
							<h5 id="id" style="border:1px solid #8cb2bc;border-radius: 5px; padding:6px;background-color: white;"
							>Województwo: <span style="color:#14467a">{{address.state}}</span></h5>
							<h5 id="id" style="border:1px solid #8cb2bc;border-radius: 5px; padding:6px;background-color: white;"
							>Kraj: <span style="color:#14467a">{{address.country}}</span></h5>
							<h5 id="id" style="border:1px solid #8cb2bc;border-radius: 5px; padding:6px;background-color: white;"
							>Kod pocztowy: <span style="color:#14467a">{{address.postalCode}}</span></h5>
							
							
							<div style="padding: 9px;">

							<button class="btn btn-primary" onclick="skasuj()">Usuń
												konto</button>
							</div>

							
							

						</div>


					</div>



				</div>
				</div>


			</div>

		</div>

	</div>
</section>

</body>
<!-- /.container -->