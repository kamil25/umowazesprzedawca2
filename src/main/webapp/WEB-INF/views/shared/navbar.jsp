<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<link href="https://fonts.googleapis.com/css?family=Lobster" rel="stylesheet">
	<script>
		window.userRole = '${userModel.role}';
	</script>
	<style> 
@font-face {
    font-family: Lob;
    src: url('${fonts}/Lobster-Regular.ttf');
    
}


</style>
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" style="font-family:'Lobster', cursive; color:#00CCFF;" href="${contextRoot}/home">UmowaZeSprzedawca.pl</a>
                
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    
                    
					
	                    <li id="dodawanieUmowy">
	                        <a href="${contextRoot}/create/umowa">Dodaj umowę</a>
	                    </li>					
					
					
					
	                    <!--<li id="listaUmow">
	                        <a href="${contextRoot}/listaUmow">Lista umów - kupno</a>
	                    </li>					
					
					
	                    <li id="listaUmowSprzedajacy">
	                        <a href="${contextRoot}/listaUmowSprzedajacy">Lista umów - sprzedaż</a>
	                    </li>-->

					<li class="dropdown" id="mojeUmowy">
						<a class="btn btn-default dropdown-toggle" href="javascript:void(0)" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
							Moje umowy
							<span class="caret"></span>
						</a>
						<ul class="dropdown-menu" aria-labelledby="dropdownMenu">
							<li id="umowaKupno">
								<a href="${contextRoot}/listaUmow">Lista umów - kupno</a>
							</li>
							<li id="umowaSprzedaz">
								<a href="${contextRoot}/listaUmowSprzedajacy">Lista umów - sprzedaż</a>
							</li>

						</ul>
					</li>

					<li id="article">
						<a href="${contextRoot}/artykuly">Artykuły</a>
					</li>
					
                </ul>


			    
			    <ul class="nav navbar-nav navbar-right">
			    	<security:authorize access="isAnonymous()">
	                    <li id="signup">
	                        <a href="${contextRoot}/membership">Rejestracja</a>
	                    </li>
						<li id="login">
	                        <a href="${contextRoot}/login">Zaloguj</a>
	                    </li> 			    	
			    	</security:authorize>
			    	<security:authorize access="isAuthenticated()">
						<li class="dropdown" id="userModel">
						  <a class="btn btn-default dropdown-toggle" href="javascript:void(0)" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
						    ${userModel.fullName}
						    <span class="caret"></span>
						  </a>
						  <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
		                    <li id="mojeKonto">
		                        <a href="${contextRoot}/mojeKonto/">Moje konto</a>
		                    </li>
							<li id="logout">
		                        <a href="${contextRoot}/logout">Wyloguj</a>
		                    </li>   
		                                     			    	
						  </ul>		
						</li>    			    
			    	</security:authorize>                    
			    </ul>                
                
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

