    <%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style>
        .fa {
            padding: 20px;
            font-size: 30px;
            width: 50px;
            text-align: center;
            text-decoration: none;
            margin: 5px 2px;
        }

        .fa:hover {
            opacity: 0.7;
        }

        .fa-facebook {
            background: #3B5998;
            color: white;
        }
    </style>
    <div class="container footer">

        <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="panel-footer">
       				<div class="nav navbar-nav">
                    <li id="about">
                        <a href="${contextRoot}/about">O aplikacji</a>
                    </li>

                    <li id="contact">
                        <a href="${contextRoot}/contact">Kontakt</a>
                    </li>
                    <li id="regulamin">
                        <a href="${contextRoot}/regulamin">Regulamin</a>
                    </li>
                        <li id="facebook">

                            <a href="https://facebook.com/UmowaZeSprzedawca/" class="fa fa-facebook" target="_blank"></a>
                        </li>
                    </div>
                    <div class="text-right">
                    </br>
                    <p>Wszelkie prawa zastrzeżone &copy; Umowa ze sprzedawcą 2018</p>
                    </div>
                </div>
            </div>
        </footer>

    </div>