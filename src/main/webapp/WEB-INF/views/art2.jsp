<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8"%>
<link href="${css}/blog.css" rel="stylesheet">
</head>
<div class="blog-header">
    <div class="container">
        <h1 class="blog-title">Blogi i felietony</h1>
        <p class="lead blog-description">Artykuły, które odkryją przed Tobą tajniki zakupów online</p>
    </div>
</div>

<div class="container">

    <div class="row">

        <div class="col-sm-8 blog-main">

            <div class="blog-post">
                <h2 class="blog-post-title">Grupy sprzedażowe z Facebooka i groźni oszuści</h2>
                <p class="blog-post-meta">Grudzień 28, 2018 autor: Paweł Poniatowski</p>

                <p>Facebook dzięki uruchomieniu usługi Marketplace i działalności grup sprzedażowych zyskał wielu użytkowników 
                i ułatwił sprzedaż niepotrzebnych przedmiotów obecnym użytkownikom. Na samym początku funkcjonowania tychże grup 
                faktycznie dochodziło jedynie do sytuacji, kiedy to zwykli użytkownicy sprzedawali przedmioty ze swojego domu, 
                artyści rękodzieła docierali do klientów ze swoimi wyrobami i 
                każdy mógł dołożyć kilka groszy do domowego budżetu. <strong>Jednak tak duży potencjał nie mógł pozostać 
                niewykorzystany przez wszelakiej maści oszustów. </strong></p>

                <h3>Jakich kont używają oszuści</h3>
                <p>Każdy użytkownik Facebooka ma możliwość zablokowania pewnych informacji dla nieznajomych użytkowników, 
                a niektóre z nich mogą nie być widoczne nawet dla znajomych. Śladowa ilość znajomych, prawie pusta tablica 
                i brak informacji do podglądu to pierwsze symptomy potencjalnego oszusta, które powinny w naszej głowie 
                zapalić czerwoną lampkę ostrzegawczą. Zdjęcia profilowe nierzadko również nie są wrzucane wcale, lub też 
                są to zdjęcia bez trudu możliwe do znalezienia po użyciu funkcji szukaj obrazu w Google grafika. 
                <strong>Co sprytniejsi oszuści kradną je również z innych profili, 
                by trudniej było wyśledzić ich niecne zamiary.</strong> Wiele imion obcego pochodzenia może również wskazywać na to, 
                że mamy doczynienia z oszustem.</p>

                <h3>Inne typowe zachowania nieuczciwych sprzedających</h3>
                <p>Kiedy już oceniliśmy konto i nie do końca mamy pewność, można zwrócić uwagę na inne symptomy. </p>
                <blockquote>
                    Na lokalnych grupach dotyczących konkretnych miast coraz częściej udzielają się osoby, 
                    które to mieszkają na drugim końcu Polski, lub nawet w innym kraju. 
                </blockquote>
                <p>Sposób ich dotarcia do niewielkiej lokalnej grupy jest zagadką. Jednak oszuści często stosują 
                taktykę dodania się do każdej grupy mającej w swojej nazwie kupię lub sprzedam. Naturalnie informacja 
                o przynależności również jest ukrywana lub dane konto służy maksymalnie do kilku grup. 
                Drugim powszechnym trikiem jest wrzucanie zdjęć towaru z Internetu. Nie mając fizycznie danego towaru, często 
                kradnie się zdjęcia z zagranicznych stron w celu zwiększenia wiarygodności. Poproszenie w wiadomości prywatnej 
                o więcej zdjęć to rozsądny pomysł. Trzecim, naturalnie wykorzystywanym na każdym portalu ogłoszeniowym sposobem 
                jest wystawienie towaru w atrakcyjnej cenie tłumaczone pilną potrzebą zdobycia gotówki i przelew ekspresowy, 
                bez możliwości nawet rozmowy o wysyłce za pobraniem. </p>
                
                <img src="${images}/social-media-763731_1920.jpg" alt="grupy-fb" class="landingImg" style="max-width: 40%; height: auto; margin-top:6px;margin-bottom: 14px;">
                

                <h3>Sposoby obrony przed oszustami </h3>
                <p>Podstawowy sposób to wspomniane poproszenie o więcej zdjęć. Kolejnym jest naleganie na podpisanie 
                <a href="${contextRoot}/home" target="_blank">umowy 
                kupna-sprzedaży</a> za pośrednictwem Internetu wraz z podaniem pełnych danych. To zawsze dodatkowe zabezpieczenie. 
                Kupując towar większej wartości powinniśmy nalegać również na wysłanie paczki za pobraniem z możliwością sprawdzenia 
                jej zawartości. Doskonałym pomysłem jest również zapytanie o miejsce zamieszkania i wskazanie, że ktoś ze znajomych 
                może pojawić się w celu odebrania paczki kolejnego dnia. Przypadkowo odnaleziony kuzyn mieszkający w pobliżu to świetny 
                sposób na odciągnięcie od chęci rozmowy oszusta. Kiedy okaże się, że jednak sprzedający jest uczciwy i może się spotkać, 
                można zawsze odwołać spotkanie z powodu wypadku losowego, wznawiając procedurę weryfikacji. Kiedy natomiast jest już 
                za późno i straciliśmy pieniądze, wraz z kompletem dokumentacji takiej jak rozmowy przez Messenger, dane konta do wpłaty 
                i potwierdzenie przelewu, niezwłocznie musimy udać się na najbliższy komisariat i 
                złożyć zawiadomienie. Warto jest też poinformować innych użytkowników Facebooka na specjalnych grupach dla oszukanych. </p>

                


            </div><!-- /.blog-post -->


            <nav class="blog-pagination">
                <a class="btn btn-outline-primary" href="${contextRoot}/artykuly">Zanim zrobisz zakupy online przeczytaj inne artykuły</a>

            </nav>

        </div><!-- /.blog-main -->

        <div class="col-sm-3 offset-sm-1 blog-sidebar">
            <div class="sidebar-module sidebar-module-inset">
                <h4>Aplikacja</h4>
                <p>Pamiętaj, że dzięki umowie online zwiększasz szanse na bezpieczne zakupy w internecie</p>
                <div class="mbr-section-btn pt-3 align-left">
                    <a class="btn btn-md btn-info display-4"
                       href="${contextRoot}/membership">Rejestracja</a>
                </div>
            </div>
        </div><!-- /.blog-sidebar -->

    </div><!-- /.row -->

</div><!-- /.container -->
